<?php
include("db/connection.php");
$obj = new servicepack();
$obj->RestrictAccess();
if (isset($_POST['name'])) {

    $data['name']  = trim($_POST['name']);
    $data['status'] = trim($_POST['status']);
    //   $data['id'] = isset($_POST['id'])?$_POST['id']:'0';
    $data['id'] = $_POST['id'];
    if (isset($_POST['id'])) {
        // print_r($_POST); exit;
        if ($obj->check_name_exists("tbrl_color", $data) == "0") $obj->update_data("tbrl_color", $data, $_POST['id']);
    } else {
        if ($obj->check_name_exists("tbrl_color", $data) == "0") $obj->insert_data("tbrl_color", $data);
    }
    header("location:color_management.php");
}
if (isset($_REQUEST['edit'])) {
    $row_e = $obj->select_by_id("tbrl_color", $_REQUEST['edit']);
}
if (isset($_REQUEST['delete'])) {

    $delete = $obj->delete_by_id("tbrl_color", $_REQUEST['delete']);
    header("location:color_management.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php include("includes/head.php"); ?>
    <title>Manage Color | Admin Dashboard</title>

</head>

<body class="fix-header card-no-border">
    <!-- <div class="preloader">
  <svg class="circular" viewBox="25 25 50 50">
  <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div> -->
    <div id="main-wrapper">
        <?php include("includes/top_bar.php"); ?>
        <?php include("includes/side_nav.php"); ?>
        <div class="page-wrapper">
            <div class="row page-titles">
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="button-box">
                            <button type="button" class="btn btn-success" id="add">ADD COLOR</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card" id="add_color" style="display:none">
                            <div class="card-body">
                                <h4 class="card-title"><?= isset($row_e) ? 'UPDATE' : 'CREATE' ?> COLOR </h4>
                                <form action="" method="post" enctype='multipart/form-data'>
                                    <div class="modal-body">
                                        <?php if (isset($row_e)) { ?>
                                            <input type="hidden" class="form-control" name="id" value="<?php echo $row_e['id'] ?>">
                                        <? } ?>
                                    </div>
                                    <div class="form-row">

                                        <div class="col-md-4 mb-3">
                                            <label for="category" class="control-label">Color</label>
                                            <input type="text" class="form-control" id="name" name="name" value="<?php if (isset($row_e['name'])) echo $row_e['name']; ?>" required>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="status" class="control-label">Status</label>
                                            <select class="form-control custom-select" data-placeholder="Choose Status" tabindex="1" name="status" required>
                                                <option value="1" <?php if (isset($row_e['status'])) echo ($row_e['status'] == "1") ? "selected" : ""; ?>>Enable</option>
                                                <option value="2" <?php if (isset($row_e['status'])) echo ($row_e['status'] == "2") ? "selected" : ""; ?>>Disable</option>
                                            </select>

                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success"><?= isset($row_e) ? 'UPDATE' : 'CREATE' ?></button>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Manage Color</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th class="text-nowrap">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 0;
                                            $result = $obj->select_all_by_status("tbrl_color", 0);
                                            while ($row = mysqli_fetch_assoc($result)) {
                                            ?>
                                                <tr>
                                                    <td><?php echo ++$i; ?></td>
                                                    <td><?php echo ucwords($row['name']); ?></td>
                                                    <td style="color:<?php echo ($row['status'] == "1") ? "green" : "red"; ?>"><?php echo ($row['status'] == "1") ? "Enable" : "Disable"; ?></td>
                                                    <td class="text-nowrap">
                                                        <a href="?edit=<?php echo $row['id'] ?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                        <a href="?delete=<?php echo $row['id'] ?>" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include("includes/footer.php"); ?>
            </div>
        </div>
    </div>
</body>
<?php include("includes/foot.php"); ?>
<script>
    <? if (isset($_REQUEST['edit'])) { ?>
        $(function() {
            $("#add_color").toggle();
        });
    <? } ?>
</script>
<script>
    $(document).ready(function() {
        $("#add").click(function() {
            $("#add_color").toggle();
        });
    });
</script>

</html>