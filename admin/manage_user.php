<?php
include ("db/connection.php");
$obj=new servicepack();
$obj->RestrictAccess();
if(isset($_POST['user_type']))
{
  $data['user_type']  = trim($_POST['user_type']);
  $data['name'] = trim($_POST['name']);
  $data['email'] = trim($_POST['email']);
  $data['phno'] = trim($_POST['phno']);
  $data['username'] = trim($_POST['username']);
  $data['password'] = trim($_POST['password']);  
  $data['status'] = trim($_POST['status']);
  $data['created_at'] = date('Y-m-d h:i:s');

  if(isset($_POST['id'])) { 
    $obj->update_data("tbl_user",$data, $_POST['id']); 
  }
  else { 
    $obj->insert_data("tbl_user",$data); 
  }
  header("location:manage_user.php");
}
if(isset($_REQUEST['edit'])){
  $row_e = $obj->select_by_id("tbl_user",$_REQUEST['edit']);
}
if(isset($_REQUEST['delete'])){
  $delete = $obj->delete_by_id("tbl_user",$_REQUEST['delete']);
  header("location:manage_user.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <? include("includes/head.php"); ?>
  <title>Manage User | Admin Dashboard</title>
  
</head>
<body class="fix-header card-no-border">
<!-- <div class="preloader">
  <svg class="circular" viewBox="25 25 50 50">
  <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div> -->
<div id="main-wrapper">
<?php include ("includes/top_bar.php"); ?>
<?php include ("includes/side_nav.php"); ?>
  <div class="page-wrapper">
    <div class="row page-titles">
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
          <div class="button-box">
             <button type="button" class="btn btn-success" id="add"><?=isset($row_e) ? 'EDIT' : 'ADD'?> </button>
          </div>
        </div>
      </div>
      <div class="card" id="add_user" style="display:none">
              <div class="card-body">
                <h4 class="card-title"><?=isset($row_e) ? 'EDIT' : 'ADD'?> </h4>
                <form action="" method="post" enctype='multipart/form-data'>
                <div class="modal-body">
                 <?php  if(isset($row_e)){ ?>
                      <input type="hidden" name="id" value="<?php echo $row_e['id']?>">
                   <?php } ?>
                   <div class="form-row">
                    <div class="col-md-4 mb-3">
                      <label for="name" class="control-label">Name</label>
                      <input type="text" class="form-control" id="name" name="name" value="<?php  if(isset($row_e['name']))echo $row_e['name']; ?>" required>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="email" class="control-label">Email</label>
                      <input type="email" class="form-control" id="email" name="email" value="<?php  if(isset($row_e['email']))echo $row_e['email']; ?>" required>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="Phone" class="control-label">Phone No</label>
                      <input type="number" class="form-control" id="phno" name="phno" value="<?php  if(isset($row_e['phno']))echo $row_e['phno']; ?>" required>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="Phone" class="control-label">Username</label>
                      <input type="text" class="form-control" id="username" name="username" value="<?php  if(isset($row_e['username']))echo $row_e['username']; ?>" required>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="Phone" class="control-label">Password</label>
                      <input type="text" class="form-control" id="password" name="password" value="<?php  if(isset($row_e['password']))echo $row_e['password']; ?>" required>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="Phone" class="control-label">User Type</label>
                      <select  class="form-control custom-select" data-placeholder="Choose user type" tabindex="1" name="user_type" required>
                        <?php
                          $result = $obj->select_all_by_status("tbl_user_type",1);
                          while($row= mysqli_fetch_assoc($result)){
                        ?>
                         <option value="<?php echo $row['id'] ?>" <?php if(isset($row_e['user_type'])) echo ($row_e['user_type'] == $row['id'])?"selected":"";?> ><?php echo ucwords($row['type']); ?></option> 
                        <?php 
                         }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="status" class="control-label">Status</label>
                      <select class="form-control custom-select" data-placeholder="Choose Status" tabindex="1" name="status" required>
                      <option value="1" <?php if(isset($row_e['id'])) echo ($row_e['status'] == "1")?"selected":"";?>>Enable</option>
                      <option value="2" <?php if(isset($row_e['id'])) echo ($row_e['status'] == "2")?"selected":"";?>>Disable</option>
                      </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><?=isset($row_e) ? 'UPDATE' : 'CREATE'?></button>
                </div>
              </div>
              </form>
            </div>
           </div>
     
      <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Manage User</h4>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>User Type</th>                    
                    <th>Email</th>
                    <th>Phone No</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th class="text-nowrap">Action</th>
                    </tr>
                  </thead>
                    <tbody>
                      <?php
                        $i=0;
                        $result = $obj->select_all_user("tbl_user");
                        while($row= mysqli_fetch_assoc($result)){
                      ?>
                      <tr>
                        <td><?php echo $i=$i+1;?></td>
                        <td><?php echo ucwords($row['name']);?></td>
                        <td><?php echo ucwords($row['type']);?></td>
                        <td><?php echo $row['email'];?></td>
                        <td><?php echo ucwords($row['phno']);?></td>
                        <td><?php echo ucwords($row['username']);?></td>
                        <td><?php echo ucwords($row['password']);?></td>
                        <td style="color:<?php echo ($row['status']=="1")?"green":"red";?>"><?php echo ($row['status']=="1")?"Active":"Delete";?></td>
                        <td class="text-nowrap">
                          <a href="?edit=<?php echo $row['id']?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                          <a href="?delete=<?php echo $row['id']?>" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                        </td>
                      </tr>
                      <?php 
                        }
                      ?>                 
                    </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("includes/footer.php"); ?>
     </div>
    </div>
  </div>
</body>
<? include("includes/foot.php"); ?>
    <script>
          <? if(isset($_REQUEST['edit'])){ ?>
              $(function() {
                    $("#add_user").toggle();
              });
          <? } ?>
      </script>
      <script>
      $(document).ready(function(){
        $("#add").click(function(){
          $("#add_user").toggle();
        });
      });
      </script>
</html>
