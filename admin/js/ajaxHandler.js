define(function(require) {
    'use strict';

    var _ = require('underscore')

    return {
        handleAjaxSuccess: function() {
            if (_.isFunction(this.closeForm)) {
                this.closeForm()
            }

            if (_.isFunction(this.triggerMethod)) {
                this.triggerMethod('ajax:success', arguments);
            }
        },

        handleAjaxError: function(model, response) {
            var view = this.region.currentView,
                responseJSON = response.responseJSON

            if (view && (responseJSON && responseJSON.message)) {
                if (_.isFunction(view.displayFormError)) {
                    view.displayFormError(responseJSON.message)
                }
            }

            if (_.isFunction(this.triggerMethod)) {
                this.triggerMethod('ajax:error', arguments);
            }
        }
    }
});
