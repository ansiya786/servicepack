<?php
include("db/connection.php");
$obj = new servicepack();
$obj->RestrictAccess();
if (isset($_POST['name'])) {
  $data['brand_id'] = $_POST['brand'];
  $data['model_id'] = $_POST['model'];
  $data['series_id'] = $_POST['series'];
  $data['color_id'] = $_POST['color'];
  $data['name']  = trim($_POST['name']);
  $data['description']  = trim($_POST['description']);
  $data['search_keyword']  = trim($_POST['search_keyword']);
  $data['part_number']  = $_POST['part_number'];
  $data['quality']  = $_POST['quality'];
  $data['packing']  = $_POST['packing'];
  $data['parts']  = $_POST['parts'];
  $data['condition']  = $_POST['condition'];
  $data['seo_title']  = trim($_POST['seo_title']);
  $data['meta_description']  = trim($_POST['meta_description']);
  $data['meta_keyword']  = trim($_POST['meta_keyword']);

  $data['status'] = trim($_POST['status']);
  $data['image'] =  $_POST['hiddenimg'];
  if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {
    $data['image'] = $obj->upload_file_attach("uploads/images/", $_FILES["image"]);
  }
  if (isset($_POST['id'])) {
    $row_e = $obj->select_by_id("tbrl_products", $_POST['id']);
    if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {
      $obj->unlinkFile('uploads/images/' . $row_e['image']);
    }
    $obj->update_data("tbrl_products", $data, $_POST['id']);
  } else {
    $obj->insert_data("tbrl_products", $data);
  }
  header("location:product_management.php");
}
if (isset($_REQUEST['edit'])) {
  $row_e = $obj->select_by_id("tbrl_products", $_REQUEST['edit']);
}
if (isset($_REQUEST['delete'])) {
  $row_e = $obj->select_by_id("tbrl_products", $_REQUEST['delete']);
  if (!empty($row_e['image'])) {
    $obj->unlinkFile('uploads/images/' . $row_e['banner_image']);
  }
  $r_d = $obj->delete_by_id("tbrl_products", $_REQUEST['delete']);
  header("location:product_management.php");
}

//   $data['id'] = isset($_POST['id']) ? $_POST['id'] : '0';
//   if (isset($_POST['id'])) {
//     if ($obj->check_name_exists("tbl_product", $data) == "0") $obj->update_data("tbl_product", $data, $_POST['id']);
//   } else {
//     if ($obj->check_name_exists("tbl_product", $data) == "0") $obj->insert_data("tbl_product", $data);
//   }
//   header("location:product_management.php");
// }
// if (isset($_REQUEST['edit'])) {
//   $row_e = $obj->select_by_id("tbl_product", $_REQUEST['edit']);
// }
// if (isset($_REQUEST['delete'])) {
//   $delete = $obj->delete_by_id("tbl_product", $_REQUEST['delete']);
//   header("location:product_management.php");
// }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <? include("includes/head.php"); ?>
  <title>Manage Products | Admin Dashboard</title>

</head>

<body class="fix-header card-no-border">
  <div id="main-wrapper">
    <?php include("includes/top_bar.php"); ?>
    <?php include("includes/side_nav.php"); ?>
    <div class="page-wrapper">
      <div class="row page-titles">
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="button-box">
              <button type="button" class="btn btn-success" id="add">ADD PRODUCT</button>
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-12">
            <div class="card" id="add_product" style="display:none">
              <div class="card-body">
                <h4 class="card-title"><?= isset($row_e) ? 'UPDATE' : 'CREATE' ?> PRODUCT</h4>
                <form action="" method="post" enctype='multipart/form-data'>
                  <div class="modal-body">
                    <?php if (isset($row_e)) { ?>
                      <input type="hidden" class="form-control" name="id" value="<?php echo $row_e['id'] ?>">
                    <?php } ?>
                  </div>
                  <div class="form-row">
                    <div class="col-md-3 mb-3">
                      <label for="brand" class="control-label">Brand</label>
                      <select class="form-control custom-select" data-placeholder="Choose Status" tabindex="1" onChange="getModel(this.value, '');" id="brand-dropdown" name="brand" required>
                        <option>-- Select Brand --</option>
                        <?php
                        $result = $obj->select_all_by_active("tbrl_brands");

                        while ($row = mysqli_fetch_assoc($result)) {
                        ?>
                          <option value="<?php echo $row['id']; ?>" <?php if (isset($row_e['brand_id'])) echo ($row_e['brand_id'] == $row['id']) ? "selected" : ""; ?>><?php echo $row['name']; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-3 mb-3">
                      <label for="model" class="control-label">Model</label>
                      <select class="form-control custom-select" data-placeholder="Choose Status" tabindex="1" id="model-dropdown" name="model" required>

                      </select>

                    </div>
                    <div class="col-md-3 mb-3">
                      <label for="name" class="control-label">Series</label>
                      <select class="form-control custom-select" data-placeholder="Choose Status" tabindex="1" id="series-dropdown" name="series" required>

                      </select>

                    </div>
                    <div class="col-md-3 mb-3">
                      <label for="color" class="control-label">Color</label>
                      <select class="form-control custom-select" data-placeholder="Choose Status" tabindex="1" name="color" required>
                        <?php
                        $result = $obj->select_all_by_active("tbrl_color");

                        while ($row = mysqli_fetch_assoc($result)) {
                        ?>
                          <option value="<?php echo $row['id']; ?>" <?php if (isset($row_e['brand_id'])) echo ($row_e['brand_id'] == $row['id']) ? "selected" : ""; ?>><?php echo $row['name']; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="col-md-4 mb-3">
                      <label for="name" class="control-label">Product Name</label>
                      <input type="text" class="form-control" id="name" name="name" value="<?php if (isset($row_e['name'])) echo $row_e['name']; ?>" required>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="description" class="control-label">Product Description</label>
                      <textarea class="form-control" id="description" name="description" required><?= isset($row_e) ? $row_e['description'] : '' ?></textarea>

                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="description" class="control-label">Search Keywords</label>
                      <textarea class="form-control" id="search_keyword" name="search_keyword" required><?= isset($row_e) ? $row_e['search_keyword'] : '' ?></textarea>

                    </div>
                    
                  </div>

                  <div class="form-row">
                    <div class="col-md-3 mb-3">
                      <label for="name" class="control-label">Part Number</label>
                      <input type="text" class="form-control" id="part_number" name="part_number" value="<?php if (isset($row_e['part_number'])) echo $row_e['part_number']; ?>" required>
                    </div>
                    <div class="col-md-3 mb-3">
                      <label for="name" class="control-label">Quality</label>
                      <input type="text" class="form-control" id="quality" name="quality" value="<?php if (isset($row_e['quality'])) echo $row_e['quality']; ?>" required>
                    </div>

                    <div class="col-md-3 mb-3">
                      <label for="name" class="control-label">Packing</label>
                      <input type="text" class="form-control" id="packing" name="packing" value="<?php if (isset($row_e['packing'])) echo $row_e['packing']; ?>" required>
                    </div>
                    <div class="col-md-3 mb-3">
                      <label for="name" class="control-label">Enter Parts</label>
                      <input type="text" class="form-control" id="parts" name="parts" value="<?php if (isset($row_e['parts'])) echo $row_e['parts']; ?>" required>
                    </div>
                    
                  </div>

                  <div class="form-row">
                    <div class="col-md-3 mb-3">
                      <label for="name" class="control-label">Condition</label>
                      <input type="text" class="form-control" id="condition" name="condition" value="<?php if (isset($row_e['condition'])) echo $row_e['condition']; ?>" required>
                    </div>
                    <div class="col-md-3 mb-3">
                      <label for="image" class="control-label">Upload Image</label>
                      <input type="file" class="form-control" id="image" name="image" accept="image/x-png,image/jpeg,image/jpg">
                      <input type="hidden" class="form-control" name="hiddenimg" value="<?= isset($row_e) ? $row_e['image'] : '' ?>">
                    </div>

                    <div class="col-md-3 mb-3">
                      <label for="category" class="control-label">SEO Title</label>
                      <input type="text" class="form-control" id="seo_title" name="seo_title" value="<?php if (isset($row_e['seo_title'])) echo $row_e['seo_title']; ?>" required>
                    </div>
                    <div class="col-md-3 mb-3">
                      <label for="category" class="control-label">Meta Description</label>
                      <input type="text" class="form-control" id="meta_description" name="meta_description" value="<?php if (isset($row_e['meta_description'])) echo $row_e['meta_description']; ?>" required>
                    </div>
                    
                  </div>

                  <div class="form-row">
                    <div class="col-md-3 mb-3">
                      <label for="category" class="control-label">Meta Keyword</label>
                      <input type="text" class="form-control" id="meta_keyword" name="meta_keyword" value="<?php if (isset($row_e['meta_keyword'])) echo $row_e['meta_keyword']; ?>" required>
                    </div>
                    <div class="col-md-3 mb-3">
                      <label for="status" class="control-label">Status</label>
                      <select class="form-control custom-select" data-placeholder="Choose Status" tabindex="1" name="status" required>
                        <option value="1" <?php if (isset($row_e['status'])) echo ($row_e['status'] == "1") ? "selected" : ""; ?>>Enable</option>
                        <option value="2" <?php if (isset($row_e['status'])) echo ($row_e['status'] == "2") ? "selected" : ""; ?>>Disable</option>
                      </select>
                    </div>
                  </div>


                  <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><?= isset($row_e) ? 'UPDATE' : 'CREATE' ?></button>
                  </div>
                </form>
              </div>
            </div>

            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Manage Products</h4>
                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Brand</th>
                        <th>Model</th>
                        <th>Series</th>
                        <th>Image</th>
                        <th>Color</th>
                        <th>Product Name</th>
                        <th>Status</th>
                        <th class="text-nowrap">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i = 0;
                      $resultfetch = $obj->select_all_products("tbrl_products");
                      while ($rowfetch = mysqli_fetch_assoc($resultfetch)) {
                      ?>
                        <tr>
                          <td><?php echo $i = $i + 1; ?></td>
                          <td><?php echo ucwords($rowfetch['brand_name']); ?></td>
                          <td><?php echo ucwords($rowfetch['model_name']); ?></td>
                          <td><?php echo ucwords($rowfetch['series_name']); ?></td>
                          <td><img src="uploads/images/<?=$rowfetch['image']?>"  width="150px"></td>
                          <td><?php echo ucwords($rowfetch['color']); ?></td>
                          <td><?php echo ucwords($rowfetch['name']); ?></td>
                          <td style="color:<?php echo ($rowfetch['status'] == "1") ? "green" : "red"; ?>"><?php echo ($rowfetch['status'] == "1") ? "Enable" : "Disable"; ?></td>
                          <td class="text-nowrap">
                            <a href="?edit=<?php echo $rowfetch['id'] ?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                            <a href="?delete=<?php echo $rowfetch['id'] ?>" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                          </td>
                        </tr>
                      <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php include("includes/footer.php"); ?>
      </div>
    </div>
  </div>
</body>
<? include("includes/foot.php"); ?>
<script>
  $(document).ready(function(){
        $("#add").click(function(){
            $("#add_product").toggle();
        });

    });

  <?php if (isset($_REQUEST['edit'])) { ?>
    $(function() {
      $("#add_product").toggle();
    });
  <?php } ?>

  <?php if(isset($row_e['model_id'])) { 
      ?>
      var brand_id = '<?php echo $row_e['brand_id'];?>';
      var model_id = '<?php echo $row_e['model_id'];?>';
      getModel(brand_id,model_id)
      <?php
     }?>

  function getModel(val,model) {
    $("#series-dropdown").html('');
    $.ajax({
      type: "POST",
      url: "model_by_brand",
      data: 'brand_id=' + val+'&model='+model,

      success: function(data) {
        $("#model-dropdown").html(data);

      }
    });
  }

  <?php if(isset($row_e['model_id'])) { 
      ?>
      var brand_id = '<?php echo $row_e['brand_id'];?>';
      var series_id = '<?php echo $row_e['series_id'];?>';
      getSeries(brand_id,series_id)
      <?php
     }?>
  
  function getSeries(val,series) {
    var brand_id = $("#brand-dropdown").val();
    // console.log(brand_id);
    $.ajax({
      type: "POST",
      url: "series_by_brand",
      data: 'brand_id=' + val+'&series='+series,

      success: function(data) {
        $("#series-dropdown").html(data);

      }
    });

  }
</script>



</html>