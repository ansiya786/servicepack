<?php
include ("db/connection.php");
$obj=new servicepack();
$obj->RestrictAccess();

if(isset($_POST['status']))
{
//   $data['caption']=addslashes($_POST['caption']);
  $data['alt']=$_POST['alt'];
  $data['type']=addslashes($_POST['type']);
  $data['seo_url']=addslashes($_POST['seo_url']);
  $data['status']=$_POST['status'];
  $data['image'] =  $_POST['hiddenimg'];
  if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {
   $data['image'] = $obj->upload_file_attach("../assets/images/products/", $_FILES["image"]);
  }
  if(isset($_POST['id'])) {
    $row_e = $obj->select_by_id("product_type",$_POST['id']);
    if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {
       $obj->unlinkFile( '../assets/images/products/' . $row_e['image']);
    }
    $obj->update_data("product_type",$data, $_POST['id']);
   }else {
    $obj->insert_data("product_type",$data);
   }
    header("location:product_type.php");
}
if(isset($_REQUEST['edit'])){
    $row_e = $obj->select_by_id("product_type",$_REQUEST['edit']);
}
if(isset($_REQUEST['delete'])){
  $row_e = $obj->select_by_id("product_type",$_REQUEST['delete']);
  if(!empty($row_e['image'])) {
   $obj->unlinkFile( '../assets/images/products/' . $row_e['image']);
  }
  $r_d = $obj->delete_by_id("product_type",$_REQUEST['delete']);
  header("location:product_type.php");
}

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <?php include("includes/head.php"); ?>
    <title>Product Slider | Admin Dashboard</title>

</head>

<body class="fix-header card-no-border">

    <div id="main-wrapper">

        <?php include ("includes/top_bar.php"); ?>
        <?php include ("includes/side_nav.php"); ?>

        <div class="page-wrapper">

        <div class="row page-titles"></div>

            <div class="container-fluid">
              <div class="row">
              <div class="col-md-4">
              <?
                $result1 = $obj->select_all("product_type");
                $count=mysqli_num_rows($result1);
                if($count<2)
                {
                ?>
                    <div class="button-box">
                        <button type="button" class="btn btn-success" id="add"><?=isset($row_e) ? 'EDIT' : 'ADD'?> PRODUCT TYPE</button>
                    </div>
                <?php 
                }
                ?>

              </div>
            </div>
                <div class="row">

                  <div class="col-12">
                      <div class="card" id="add_type" style="display:none">
                          <div class="card-body">
                            <h4 class="card-title"><?=isset($row_e) ? 'EDIT' : 'ADD'?> </h4>
                            <form action="" method="post" enctype='multipart/form-data'>
                            <div class="modal-body">
                              <?  if(isset($row_e)){ ?>
                                      <input type="hidden" name="id" value="<?=$row_e['id']?>">
                                  <? } ?>

                                    <div class="form-row">

                                    <div class="col-md-4 mb-3">
                                        <label for="image" class="control-label">Image</label>
                                        <input type="file" class="form-control" id="image" name="image" accept="image/x-png,image/jpeg">
                                        <input type="hidden" class="form-control" name="hiddenimg" value="<?=isset($row_e) ? $row_e['image'] : ''?>">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="alt" class="control-label">Alt</label>
                                      <input type="text" name="alt" class="form-control" value="<?=isset($row_e) ? $row_e['alt'] : ''?>" required>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="alt" class="control-label">Type</label>
                                      <input type="text" name="type" class="form-control" value="<?=isset($row_e) ? $row_e['type'] : ''?>" required>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="alt" class="control-label">Seo url</label>
                                      <input type="text" name="seo_url" class="form-control" value="<?=isset($row_e) ? $row_e['seo_url'] : ''?>" required>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="status" class="control-label">Status</label>

                                            <select class="form-control custom-select" data-placeholder="Choose Status" tabindex="1" name="status" required>
                                              <option value="1" <?php if(isset($row_e['status'])) echo ($row_e['status'] == "1")?"selected":"";?>>Enable</option>
                                              <option value="2" <?php if(isset($row_e['status'])) echo ($row_e['status'] == "2")?"selected":"";?>>Disable</option>

                                            </select>

                                    </div>
                                  </div>

                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><?=isset($row_e) ? 'UPDATE' : 'CREATE'?></button>
                            </div>
                          </form>
                        </div>
                      </div>


                      <div class="card">
                          <div class="card-body">

                              <div class="table-responsive">
                                  <table class="table table-striped">
                                      <thead>
                                          <tr>
                                              <th>Sl.no</th>
                                              <th>Image</th>
                                              <th>Alt</th>
                                              <th>Type</th>
                                              <th>Status</th>
                                              <th class="text-nowrap">Action</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php

                                      $result = $obj->select_all("product_type");
                                      $i=1;
                                      while($row= mysqli_fetch_assoc($result)){
                                       
                                      ?>
                                          <tr>
                                              <td><?= $i++;?></td>
                                              <td><img src="../assets/images/products/<?=$row['image']?>" height="100px" width="100px"></td>
                                              <td><?=$row['alt']?></td>
                                              <td><?=stripslashes($row['type'])?></td>
                                              <td style="color:<?php echo ($row['status']=="1")?"green":"red";?>"><?php echo ($row['status']=="1")?"Active":"Delete";?></td>
                                              <td class="text-nowrap">
                                                  <a href="?edit=<?=$row['id']?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                  <a href="?delete=<?=$row['id']?>" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                              </td>
                                          </tr>
                                          <?php
                                         }
                                        ?>

                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>

                </div>



            </div>



            <? include("includes/footer.php"); ?>

        </div>

    </div>

    <?php include("includes/foot.php"); ?>

    <script>
          <? if(isset($_REQUEST['edit'])){ ?>
              $(function() {
                    $("#add_type").toggle();
              });
          <? } ?>
      </script>
      <script>
      $(document).ready(function(){
        $("#add").click(function(){
          $("#add_type").toggle();
        });
      });
      </script>



</body>

</html>
