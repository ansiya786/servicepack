<?php
include ("db/connection.php");
$obj=new servicepack();
$obj->RestrictAccess();
if(isset($_POST['user_type']))
{
  $data['type']  = trim($_POST['user_type']);
  $data['status'] = trim($_POST['status']);
  $data['id'] = isset($_POST['id'])?$_POST['id']:'0';

  if(isset($_POST['id'])) { 
    if ($obj->check_user_type_exists("tbl_user_type",$data) == "0") $obj->update_data("tbl_user_type",$data, $_POST['id']); 
  }
  else { 
    if ($obj->check_user_type_exists("tbl_user_type",$data) == "0") $obj->insert_data("tbl_user_type",$data); 
  }
  header("location:manage_user_role.php");
}
if(isset($_REQUEST['edit'])){
  $row_e = $obj->select_by_id("tbl_user_type",$_REQUEST['edit']);
}
if(isset($_REQUEST['delete'])){
  $delete = $obj->delete_by_id("tbl_user_type",$_REQUEST['delete']);
  header("location:manage_user_role.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <? include("includes/head.php"); ?>
  <title>Manage User Role | Admin Dashboard</title>
  
</head>
<body class="fix-header card-no-border">
<!-- <div class="preloader">
  <svg class="circular" viewBox="25 25 50 50">
  <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div> -->
<div id="main-wrapper">
<?php include ("includes/top_bar.php"); ?>
<?php include ("includes/side_nav.php"); ?>
  <div class="page-wrapper">
    <div class="row page-titles">
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
          <div class="button-box">
             <button type="button" class="btn btn-success" id="add"><?=isset($row_e) ? 'EDIT' : 'ADD'?> </button>
          </div>
        </div>
      </div>
      <div class="row">
      <div class="col-12">
          <div class="card" id="add_role" style="display:none">
              <div class="card-body">
                <h4 class="card-title"><?=isset($row_e) ? 'EDIT' : 'ADD'?> </h4>
                <form action="" method="post" enctype='multipart/form-data'>
                <div class="modal-body">
                  <?  if(isset($row_e)){ ?>
                    <input type="hidden" name="id" value="<?=$row_e['id']?>">
                  <? } ?>
                    <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="status" class="control-label">User Type</label>
                        <input type="text" class="form-control" id="user_type" name="user_type" value="<?php  if(isset($row_e['type']))echo $row_e['type']; ?>" required>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="status" class="control-label">Status</label>
                        <select class="form-control custom-select" data-placeholder="Choose Status" tabindex="1" name="status" required>
                        <option value="1" <?php if(isset($row_e['status'])) echo ($row_e['status'] == "1")?"selected":"";?>>Enable</option>
                        <option value="2" <?php if(isset($row_e['status'])) echo ($row_e['status'] == "2")?"selected":"";?>>Disable</option>
                        </select>
                    </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><?=isset($row_e) ? 'UPDATE' : 'CREATE'?></button>
                </div>
              </form>
            </div>
        
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Manage User Role</h4>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>User Type</th>
                    <th>Status</th>
                    <th class="text-nowrap">Action</th>
                    </tr>
                  </thead>
                    <tbody>
                      <?php
                        $i=0;
                        $result = $obj->select_all_by_status("tbl_user_type",0);
                        while($row= mysqli_fetch_assoc($result)){
                      ?>
                      <tr>
                        <td><?php echo $i=$i+1;?></td>
                        <td><?php echo ucwords($row['type']);?></td>
                        <td style="color:<?php echo ($row['status']=="1")?"green":"red";?>"><?php echo ($row['status']=="1")?"Active":"Delete";?></td>
                        <td class="text-nowrap">
                          <a href="?edit=<?php echo $row['id']?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                          <a href="?delete=<?php echo $row['id']?>" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                        </td>
                      </tr>
                      <?php 
                        }
                      ?>                 
                    </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("includes/footer.php"); ?>
     </div>
    </div>
  </div>
</body>
<? include("includes/foot.php"); ?>
   <script>
          <? if(isset($_REQUEST['edit'])){ ?>
              $(function() {
                    $("#add_role").toggle();
              });
          <? } ?>
      </script>
      <script>
      $(document).ready(function(){
        $("#add").click(function(){
          $("#add_role").toggle();
        });
      });
      </script>
</html>
