<?php
date_default_timezone_set("Asia/Dubai");
session_start();
class servicepack
{

    var $con; //global variable for connection;

    var $host = "localhost"; //host Address;
    var $user = "root"; //database User;
    var $pswd = ""; //database Password;
    var $db = "servicepack_new"; //database name;
    var $base_url = "http://localhost/servicepack/";
    var $admin_url = "http://localhost/servicepack/admin/";


    // var $host = "localhost"; //host Address;
    // var $user = "serviack_servicepack"; //database User;
    // var $pswd = "Jnu*4uP&SI($"; //database Password;
    // var $db = "serviack_servicepack"; //database name;
    // var $base_url = "https://servicepack.online/demo/";
    // var $admin_url = "https://servicepack.online/demo/admin/";


    /*..............................................*/
    function __construct()
    {
        // print_r($_SERVER['REQUEST_URI']);exit;
        $this->con = mysqli_connect($this->host, $this->user, $this->pswd, $this->db) or die("connection error");
        $this->sitekey = "6LdKUdEaAAAAAAviUjrLhaxMLkWCMdVgFQvp-GGS";
        $this->siterecaptchkey = '6LdKUdEaAAAAAAoXo2nUzD_gteWbHMiA2y1bkIP1';
        $this->page = basename($_SERVER['PHP_SELF']);
    }

    function RestrictAccess()
    {

        if (!isset($_SESSION['id'])) {
            header("location:" . $this->admin_url);
            exit;
        }
    }


    function rename_image($file_name)
    {
        $temp = explode(".", $file_name);
        $todays_date = date("mdYHis");
        $file_name = $temp[0] . "-" . $todays_date . "." . $temp[1];
        return $file_name;
    }
    function select_all($table)
    {
        $result = $this->con->query("select * from $table") or die(mysqli_error($this->con));
        return $result;
    }
    function select_all_by_desc($table)
    {
        $result = $this->con->query("select * from $table ORDER BY id DESC") or die(mysqli_error($this->con));
        return $result;
    }
    function select_all_by_active($table)
    {
        $result = $this->con->query("select * from $table where status=1") or die(mysqli_error($this->con));
        return $result;
    }

    function select_all_brand($table)
    {
        $result = $this->con->query("select * from $table where status=1") or die(mysqli_error($this->con));
        return $result;
    }
    function select_all_colors($table)
    {
        $result = $this->con->query("select * from $table where status=1") or die(mysqli_error($this->con));
        return $result;
    }


    function select_all_brands_by_active($table, $id)
    {
        $result = $this->con->query("select * from $table where status=1 and type_id=$id order by sort_order") or die(mysqli_error($this->con));
        return $result;
    }
    function select_all_active_by_order($table, $where = "")
    {
        $result = $this->con->query("select * from $table where status=1  $where order by sort_order ") or die(mysqli_error($this->con));
        return $result;
    }
    function select_all_active_order_by_id($table, $id)
    {
        $result = $this->con->query("select * from $table where status=1  and type_id=$id order by sort_order") or die(mysqli_error($this->con));
        return $result;
    }


    function select_all_active($table)
    {
        $result = $this->con->query("select * from $table where status=1 ORDER BY sort_order") or die(mysqli_error($this->con));
        return $result;
    }

    function select_all_by_sort_order($table, $where = " WHERE 1=1 ")
    {
        $result = $this->con->query("select * from $table $where order by sort_order") or die(mysqli_error($this->con));
        return $result;
    }

    function select_all_type_by_sort_order($table, $where = " WHERE 1=1 ")
    {
        $result = $this->con->query("select * from $table $where order by main_id, sort_order") or die(mysqli_error($this->con));
        return $result;
    }

    function select_by_id($table, $id)
    {
        $result = $this->con->query("select * from $table where id =$id") or die(mysqli_error($this->con));
        return mysqli_fetch_assoc($result);
    }
    function select_by_field($table, $field, $id)
    {
        $result = $this->con->query("select * from $table where $field ='$id'") or die(mysqli_error($this->con));
        return mysqli_fetch_assoc($result);
    }
    function select_by_category($table, $field, $id)
    {
        $result = $this->con->query("select * from $table where $field ='$id'") or die(mysqli_error($this->con));
        return $result;
    }

    function select_by_id_field($table, $id)
    {
        $result = $this->con->query("select * from $table where product_id ='$id'") or die(mysqli_error($this->con));
        return $result;
    }

    function select_products_by_division($table, $id)
    {
        $result = $this->con->query("select * from $table where main_division_id =$id and status=1 order by sort_order") or die(mysqli_error($this->con));
        return $result;
    }
    function select_by_result($table, $id)
    {
        $result = $this->con->query("select * from $table where id =$id and status=1 order by sort_order") or die(mysqli_error($this->con));
        return $result;
    }
    function delete_by_id($table, $id)
    {
        $result = $this->con->query("delete from $table where id =$id");
        return $result;
    }
    function delete_by_where($table, $where = "1!=1")
    {
        $result = $this->con->query("delete from $table  where $where") or die(mysqli_error($this->con));
        return $result;
    }
    function select_id_by_name($table, $name)
    {
        $result = $this->con->query("select id from $table where name ='$name'") or die(mysqli_error($this->con));
        $row = mysqli_fetch_assoc($result);
        return $row['id'];
    }
    function select_name_by_id($table, $id)
    {
        $result = $this->con->query("select name from $table where id ='$id'") or die(mysqli_error($this->con));
        $row = mysqli_fetch_assoc($result);
        return $row['name'];
    }
    function select_image_by_id($table, $id)
    {
        $result = $this->con->query("select image from $table where id ='$id'") or die(mysqli_error($this->con));
        $row = mysqli_fetch_assoc($result);
        return $row['image'];
    }
    function select_by_condition($table, $where)
    {
        $result = mysqli_query($this->con, "select * from $table where " . $where);
        return $result;
    }
    function change_status($table, $status, $id)
    {
        $this->con->query("update  $table set status = '$status' where id=$id ") or die(mysqli_error($this->con));
    }

    function select_all_active_by_field($table, $field, $id)
    {
        $result = $this->con->query("select * from $table where $field ='$id' AND status=1 ORDER BY sort_order ASC") or die(mysqli_error($this->con));
        return $result;
    }


    function escape($string)
    {
        return mysqli_real_escape_string($this->con, trim($string));
    }


    function insert_data($table_name, $form_data)
    {
        // retrieve the keys of the array (column titles)
        $fields = array_keys($form_data);
        // build the query
        $sql = "INSERT INTO " . $table_name . "

        (`" . implode('`,`', $fields) . "`)

        VALUES('" . implode("','", $form_data) . "')";
        // run and return the query result resource
        $this->con->query($sql) or die(mysqli_error($this->con));
        return mysqli_insert_id($this->con);
    }
    // again where clause is left optional
    function update_data($table_name, $form_data, $id = '')
    {
        // check for optional where clause
        $whereSQL = '';
        if (!empty($id)) {
            $whereSQL = " WHERE id=" . $id;
        }
        // start the actual SQL statement
        $sql = "UPDATE " . $table_name . " SET ";
        // loop and build the column /
        $sets = array();
        foreach ($form_data as $column => $value) {
            $sets[] = "`" . $column . "` = '" . $value . "'";
        }
        $sql .= implode(', ', $sets);
        // append the where statement
        $sql .= $whereSQL;
        // echo $sql; exit;
        // run and return the query result
        $this->con->query($sql) or die(mysqli_error($this->con));

        return $id;
    }
    function random_home()
    {
        $result = $this->con->query("SELECT * FROM `app_home` WHERE status=1 ORDER BY RAND() LIMIT 1");
        $row = mysqli_fetch_assoc($result);
        return $row;
    }



    function check_userExists($username, $phone)
    {
        $result = $this->con->query("select * from tbl_user where username='" . $username . "' OR  phone='" . $phone . "'") or die(mysqli_error($this->con));
        $cnt = mysqli_num_rows($result);
        return $cnt;
    }
    // function send_credentils($username) {
    //     $result = $this->con->query("select * from tbl_user where username='" . $username . "' LIMIT 1") or die(mysqli_error($this->con));
    //     if (mysqli_num_rows($result) > 0) {
    //         $row = mysqli_fetch_assoc($result);
    //         $subject = "HEATZ login credentials";
    //         $message = "Hi " . $row['name'] . "<br/>";
    //         $message.= "Thank you for touch with us, your login credentials are below<br/><br/>";
    //         $message.= "<b>Email:</b> " . $row['email'] . "<br/>";
    //         $message.= "<b>Password:</b> " . $row['password'] . "<br/>";
    //         // Always set content-type when sending HTML email
    //         $headers = "MIME-Version: 1.0" . "\r\n";
    //         $headers.= "Content-type:text/html;charset=UTF-8" . "\r\n";
    //         // More headers
    //         $headers.= 'From: HEATZ<info@heatz.store>' . "\r\n";
    //         if (mail($email, $subject, $message, $headers)) {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     } else {
    //         return false;
    //     }
    // }


    function select_id_by_model_name($name)
    {
        $result = $this->con->query("select id from products where name='$name' ") or die(mysqli_error($this->con));
        $row = mysqli_fetch_assoc($result);
        if ($row) return $row['id'];
        return false;
    }

    // ---------------------------------------
    function den_login($username, $password)
    {
        $username = $this->con->real_escape_string($username);
        $password = $this->con->real_escape_string($password);

        $result = $this->con->query("Select * from tbl_user where username='$username' && password ='$password' && status = 1 ") or die(mysqli_error($this->con));
        return mysqli_fetch_assoc($result);
    }

    function select_all_logos()
    {
        $result = $this->con->query("select brands.id as logoid, brands.image as image,brands.alt as alt, brands.sort_order as sort_order ,
        brand_type.name as type, brands.link as link, brands.status as status from brands inner join brand_type on brands.type_id=brand_type.id order by sort_order");
        return $result;
    }

    function select_all_by_status($table, $status)
    {
        $wheresql = "";
        if ($status == 1) $wheresql = "where status=1";
        $result = $this->con->query("select * from  $table " . $wheresql . "") or die(mysqli_error($this->con));
        return $result;
    }
    function get_user($table)
    {
        $result = $this->con->query("select * from  $table where id='" . $_SESSION['id'] . "'") or die(mysqli_error($this->con));
        return mysqli_fetch_assoc($result);
    }
    function check_user_type_exists($table, $data)
    {
        $wheresql = "";
        if ($data['id'] != 0) $wheresql = "and id!='" . $data['id'] . "'";
        $result = $this->con->query("select * from  $table where type='" . $data['type'] . "' " . $wheresql . "") or die(mysqli_error($this->con));
        return mysqli_num_rows($result);
    }
    function select_all_user($table)
    {
        $result = $this->con->query("select $table.*,tbl_user_type.type from  $table inner join tbl_user_type on $table.user_type=tbl_user_type.id ") or die(mysqli_error($this->con));
        return $result;
    }

    function check_name_exists($table, $data)
    {
        $wheresql = "";
        if ($data['id'] != 0) $wheresql = "and id!='" . $data['id'] . "'";
        $result = $this->con->query("select * from  $table where name='" . $data['name'] . "' " . $wheresql . "") or die(mysqli_error($this->con));
        return mysqli_num_rows($result);
    }

    function upload_file_attach($path, $file)
    {

        if ($file["error"] == 0) {
            $file_name = $file["name"];
            $file_name = $this->rename_file($path, $file_name);

            if (move_uploaded_file($file["tmp_name"], $path . $file_name)) {
                return $file_name;
            }
        }

        return null;
    }

    function rename_file($upload_folder, $file_name)
    {

        $file_name = preg_replace('/\s+/', '_', $file_name);
        $actual_name = pathinfo($file_name, PATHINFO_FILENAME);
        $original_name = $actual_name . rand(100, 999) . time();
        $extension = pathinfo($file_name, PATHINFO_EXTENSION);
        // $file_name = $original_name .  "." . $extension;
        $j = 1;
        while (file_exists($upload_folder . $actual_name . "." . $extension)) {
            $actual_name = (string) $original_name . "_" . $j;
            $file_name = $actual_name . "." . $extension;
            $j++;
        }
        return $file_name;
    }


    function unlinkFile($image)
    {

        if (file_exists($image)) {
            unlink($image);
        }
    }

    function select_all_brands($table)
    {
        $result = $this->con->query("select $table.*,tbrl_brands.name as brand_name from $table inner join tbrl_brands on $table.brand_id=tbrl_brands.id ") or die(mysqli_error($this->con));
        return $result;
    }
    function select_all_series($table)
    {
        $result = $this->con->query("SELECT $table.*,tbrl_brands.name as brand_name FROM $table inner join `tbrl_brands` on $table.brand_id=tbrl_brands.id;") or die(mysqli_error($this->con));
        return $result;
    }
    function select_all_model_by_brand($table, $id)
    {
        $result = $this->con->query("select * from $table where brand_id=$id and status=1") or die(mysqli_error($this->con));
        return $result;
    }
    function select_all_products($table)
    {
        $result = $this->con->query("SELECT p.*, b.name as brand_name, m.name as model_name,s.name as series_name, c.name as color FROM tbrl_products as p inner join tbrl_brands as b on p.brand_id=b.id inner join tbrl_brand_model as m on p.model_id=m.id inner join tbrl_brand_series as s on p.series_id=s.id inner join tbrl_color as c on p.color_id=c.id where p.status=1") or die(mysqli_error($this->con));
        return $result;
    }
    function select_all_products_by_displayname($name)
    {
        $sql = "SELECT p.*, b.name as brand_name, m.name as model_name,s.name as series_name FROM tbrl_products as p inner join tbrl_brands as b on p.brand_id=b.id inner join tbrl_brand_model as m on p.model_id=m.id inner join tbrl_brand_series as s on p.series_id=s.id inner join tbrl_color as c on p.color_id=c.id WHERE p.name LIKE '%$name%' or b.name LIKE '%$name%' or m.name LIKE '%$name%' or s.name LIKE '%$name%' or p.part_number LIKE '%$name%' LIMIT 25";
        $result = $this->con->query($sql);
        return $result;
    }
    function select_all_products_by_displaynames($name)
    {
        $sql = "SELECT p.*, b.name as brand_name, m.name as model_name,s.name as series_name FROM tbrl_products as p inner join tbrl_brands as b on p.brand_id=b.id inner join tbrl_brand_model as m on p.model_id=m.id inner join tbrl_brand_series as s on p.series_id=s.id inner join tbrl_color as c on p.color_id=c.id WHERE p.name LIKE '%$name%' or b.name LIKE '%$name%' or m.name LIKE '%$name%' or s.name LIKE '%$name%' or p.part_number LIKE '%$name%'  LIMIT 10";
        $result = $this->con->query($sql);
        return $result;
    }

    function select_all_products_by_id($table,$id)
    {
        
        $result = $this->con->query("SELECT $table.*,s.name as series_name,tbrl_color.name as color_name, tbrl_brands.name as brand_name, tbrl_brand_model.name as model_name from tbrl_products
                                    inner join tbrl_brands on tbrl_products.brand_id=tbrl_brands.id 
                                    inner join tbrl_brand_model on tbrl_products.model_id=tbrl_brand_model.id 
                                    inner join tbrl_color on tbrl_products.color_id=tbrl_color.id 
                                    inner join tbrl_brand_series as s on tbrl_products.series_id=s.id
                                    where tbrl_products.id=$id and tbrl_products.status=1") or die(mysqli_error($this->con));
        return mysqli_fetch_assoc($result);
    }
    // function getSerialNo($table)
    // {

    //     $result = $this->con->query("SELECT id FROM $table ORDER BY id LIMIT 1") or die(mysqli_error($this->con));
    //     $row = mysqli_fetch_assoc($result);
    //     $id = ($row['id']?$row['id']+1:1);
    //     return "ENQ-".$id;
    // }

    function getEnquirylistByEnqid($id)
    {

        $result = $this->con->query("SELECT item.*,b.name as brand_name,m.name as model_name,p.display_name as display_name FROM tbl_enquiry as parent LEFT JOIN tbl_enquiry_item as item ON parent.id = item.parent_id LEFT JOIN tbl_product as p ON p.id = item.product_id LEFT JOIN tbrl_brands as b ON b.id = p.brand_id LEFT JOIN tbrl_brand_model as m ON m.id = p.model_id where parent.id = $id") or die(mysqli_error($this->con));
        return $result;
    }
    function check_exists($table, $where)
    {
        $result = $this->con->query("SELECT id FROM $table WHERE $where") or die(mysqli_error($this->con));
        $row = mysqli_fetch_assoc($result);
        return ($row['id'] ? $row['id'] : 0);
    }


    function select_all_by_type()
    {
        $result = $this->con->query("select product_category.*, product_type.type as type_name from product_category inner join product_type on product_category.type_id = product_type.id  ORDER BY product_category.sort_order") or die(mysqli_error($this->con));
        return $result;
    }
    function select_all_by_type_active()
    {
        $result = $this->con->query("select product_category.*, product_type.type as type_name from product_category inner join product_type on product_category.type_id = product_type.id where product_category.status=1 ORDER BY product_category.sort_order") or die(mysqli_error($this->con));
        return $result;
    }
    function select_id_by_seourl($table, $url)
    {
        $result = mysqli_query($this->con, "select * from $table where seo_url = '$url' ");
        return $result;
    }

    function select_by_typeid($table, $id)
    {
        $result = mysqli_query($this->con, "select * from $table where type_id=$id and status=1 ORDER BY sort_order");
        return $result;
    }
    function usersemailcheck($table, $email)
    {
        $result = $this->con->query("SELECT id FROM $table WHERE email = '" . $email . "'") or die(mysqli_error($this->con));
        $row = mysqli_fetch_assoc($result);
        return ($row['id'] ? $row['id'] : 0);
    }


    function getAllProducts($row, $rowperpage)
    {
        $result = $this->con->query("SELECT p.*, b.name as brand_name, m.name as model_name,s.name as series_name, c.name as color FROM tbrl_products as p inner join tbrl_brands as b on p.brand_id=b.id inner join tbrl_brand_model as m on p.model_id=m.id inner join tbrl_brand_series as s on p.series_id=s.id inner join tbrl_color as c on p.color_id=c.id limit " . $row . "," . $rowperpage) or die(mysqli_error($this->con));
        return $result;
    }

    function getAllProductsCount()
    {
        $allcount_query = "SELECT p.*, b.name as brand_name, m.name as model_name,s.name as series_name, c.name as color FROM tbrl_products as p inner join tbrl_brands as b on p.brand_id=b.id inner join tbrl_brand_model as m on p.model_id=m.id inner join tbrl_brand_series as s on p.series_id=s.id inner join tbrl_color as c on p.color_id=c.id";
        $result1 = $this->con->query($allcount_query);
        $totalRecords = mysqli_num_rows($result1);
        return $totalRecords;
    }

    function getProductsBySeriesId($id)
    {
    $result = $this->con->query("SELECT *,s.name as series_name,tbrl_color.name as color_name, tbrl_brands.name as brand_name,tbrl_products.id as productid,tbrl_products.name as productname,tbrl_brand_model.name as brandmodel FROM tbrl_products 
    inner join tbrl_brands on tbrl_products.brand_id=tbrl_brands.id 
    inner join tbrl_brand_model on tbrl_products.model_id=tbrl_brand_model.id
    inner join tbrl_color on tbrl_products.color_id=tbrl_color.id 
    inner join tbrl_brand_series as s on tbrl_products.series_id=s.id
     WHERE tbrl_products.series_id = $id and tbrl_products.status=1") or die(mysqli_error($this->con));
    return $result;

    }






    function checkNewsletterEmailexists($email_id)
    {
        $result = mysqli_query($this->con, "SELECT * FROM `tbrl_newsletter` WHERE `email` = '" . $email_id . "'");
        return $result;
    }


    function addNewsletter($data)
    {
        $insertId = 0;
        $result = mysqli_query($this->con, "SELECT * FROM  tbrl_newsletter where email ='" . $data['email'] . "' ");
        if (mysqli_num_rows($result) == 0) {
            $query = "INSERT INTO tbrl_newsletter(email,status,created_at)VALUES ('" . $data['email'] . "',1,'" . date('Y-m-d H:i:s') . "')";

            $this->con->query($query) or die(mysqli_error($this->con));
            $insertId = mysqli_insert_id($this->con);
        }
        return $insertId;
    }
    function getLatestProducts()
    {
        $result = $this->con->query("SELECT p.*, b.name as brand_name, m.name as model_name,s.name as series_name, c.name as color FROM tbrl_products as p inner join tbrl_brands as b on p.brand_id=b.id inner join tbrl_brand_model as m on p.model_id=m.id inner join tbrl_brand_series as s on p.series_id=s.id inner join tbrl_color as c on p.color_id=c.id ORDER BY p.id DESC LIMIT 16") or die(mysqli_error($this->con));
        return $result;
    }
}
