<?php
include ("db/connection.php");
$obj=new servicepack();
$obj->RestrictAccess();
if(isset($_REQUEST['delete'])){
    $delete = $obj->delete_by_id("tbrl_newsletter",$_REQUEST['delete']);
    header("location:newsletter_email.php");
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("includes/head.php"); ?>
  <title>Enquiry | Admin Dashboard</title>
  
</head>
<body class="fix-header card-no-border">

<div id="main-wrapper">
<?php include ("includes/top_bar.php"); ?>

<?php include ("includes/side_nav.php"); ?>
  <div class="page-wrapper">
    <div class="row page-titles">
    </div>
    <div class="container-fluid">
    

        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Newsletter Email</h4>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Email</th>
                    <th>Created Date</th>
                    <th>Status</th>
                    <th class="text-nowrap">Action</th>
                    </tr>
                  </thead>
                    <tbody>
                      <?php
                        $i=0;
                        $resultfetch = $obj->select_all("tbrl_newsletter");
                        while($rowfetch= mysqli_fetch_assoc($resultfetch)){
                      ?>
                      <tr>
                        <td><?php echo $i=$i+1;?></td>
                        <td><?php echo ucwords($rowfetch['email']);?></td>
                        <td><?=date('d-m-Y', strtotime( $rowfetch['created_at'] ) );?></td>                       
                        <td> <label class="switch">
                                <?php if($rowfetch['status'] == 1): ?>
                                    <input id="cheak" data-id="<?php echo $rowfetch['id']; ?>" value="1" type="checkbox" checked>
                                <?php else: ?>
                                    <input id="cheak" data-id="<?php echo $rowfetch['id']; ?>" value="0" type="checkbox">
                                <?php endif ?>
                                <span class="slider round"></span>
                        </label>
                        </td>
                        <td class="text-nowrap">
                          <a href="?delete=<?php echo $rowfetch['id']?>" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                        </td>
                     
                      </tr>
                      <?php
                        }
                      ?>
                    </tbody>
                </table>
              </div>
            </div>

            
          </div>
        </div>
      </div>

      <?php include("includes/footer.php"); ?>
     </div>
    </div>
  </div>
</body>
<? include("includes/foot.php"); ?>
<script>
    $(document).on("change","#cheak",function(e){
        var status = $(this).val();
        var id = $(this).data('id');
        if(status == 1){
            status = 2;
        }else{
            status = 1;
        }
        $.ajax({
            url : "status.php",
            type : "POST",
            data : {status:status,id:id},
            success : function(data){
            }
         });
    });

function viewProductItems(id)
{
  var enq_id = id;
  $.ajax({
    url: "viewProductItemsbyEnquiry",
    type: "POST",
    data: {
      enq_id:enq_id,

    },
    cache: false,
    success: function(dataResult){

      $('#myModal').modal('show'); 
      $("#txtdata").html(dataResult);

    }
  });

}
    </script>
   
   

</html>
