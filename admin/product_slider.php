<?php
include ("db/connection.php");
$obj=new servicepack();
$obj->RestrictAccess();

if(isset($_POST['status']))
{
  $data['content1']=addslashes($_POST['content1']);
  $data['content2']=addslashes($_POST['content2']);
  $data['alt']=$_POST['alt'];
  $data['sort_order']=$_POST['sortorder'];
  $data['status']=$_POST['status'];
  $data['banner_image'] =  $_POST['hiddenimg'];
  if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {
   $data['banner_image'] = $obj->upload_file_attach("../img/slider/", $_FILES["image"]);
  }
  if(isset($_POST['id'])) {
    $row_e = $obj->select_by_id("product_slider",$_POST['id']);
    if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {
       $obj->unlinkFile( '../img/slider/' . $row_e['banner_image']);
    }
    $obj->update_data("product_slider",$data, $_POST['id']);
   }else {
    $obj->insert_data("product_slider",$data);
   }
    header("location:product_slider.php");
}
if(isset($_REQUEST['edit'])){
    $row_e = $obj->select_by_id("product_slider",$_REQUEST['edit']);
}
if(isset($_REQUEST['delete'])){
  $row_e = $obj->select_by_id("product_slider",$_REQUEST['delete']);
  if(!empty($row_e['banner_image'])) {
   $obj->unlinkFile( '../img/slider/' . $row_e['banner_image']);
  }
  $r_d = $obj->delete_by_id("product_slider",$_REQUEST['delete']);
  header("location:product_slider.php");
}

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <?php include("includes/head.php"); ?>
    <title>Product Slider | Admin Dashboard</title>

</head>

<body class="fix-header card-no-border">

    <div id="main-wrapper">

        <?php include ("includes/top_bar.php"); ?>
        <?php include ("includes/side_nav.php"); ?>

        <div class="page-wrapper">

        <div class="row page-titles"></div>

            <div class="container-fluid">
              <div class="row">
              <div class="col-md-4">
                          <div class="button-box">
                            <button type="button" class="btn btn-success" id="add"><?=isset($row_e) ? 'EDIT' : 'ADD'?> </button>
                          </div>

              </div>
            </div>
                <div class="row">

                  <div class="col-12">
                      <div class="card" id="add_slider" style="display:none">
                          <div class="card-body">
                            <h4 class="card-title"><?=isset($row_e) ? 'EDIT' : 'ADD'?> </h4>
                            <form action="" method="post" enctype='multipart/form-data'>
                            <div class="modal-body">
                              <?  if(isset($row_e)){ ?>
                                      <input type="hidden" name="id" value="<?=$row_e['id']?>">
                                  <? } ?>

                                    <div class="form-row">

                                    <div class="col-md-4 mb-3">
                                        <label for="image" class="control-label">Banner Image</label>
                                        <input type="file" class="form-control" id="image" name="image" accept="image/x-png,image/jpeg,image/jpg">
                                        <input type="hidden" class="form-control" name="hiddenimg" value="<?=isset($row_e) ? $row_e['banner_image'] : ''?>">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="alt" class="control-label">Alt</label>
                                      <input type="text" name="alt" class="form-control" value="<?=isset($row_e) ? $row_e['alt'] : ''?>" required>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="message-text" class="control-label">Content 1</label>
                                        <textarea class="form-control" id="content1" name="content1" required><?=isset($row_e) ? $row_e['content1'] : ''?></textarea>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="message-text" class="control-label">Content 2</label>
                                        <textarea class="form-control" id="content2" name="content2" required><?=isset($row_e) ? $row_e['content2'] : ''?></textarea>
                                    </div>
                                   
                                    <div class="col-md-4 mb-3">
                                        <label for="sort-order" class="control-label">Sort Order</label>
                                      <input type="text" name="sortorder" class="form-control"  value="<?=isset($row_e) ? $row_e['sort_order'] : ''?>" required>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="status" class="control-label">Status</label>

                                            <select class="form-control custom-select" data-placeholder="Choose Status" tabindex="1" name="status" required>
                                              <option value="1" <?php if(isset($row_e['status'])) echo ($row_e['status'] == "1")?"selected":"";?>>Enable</option>
                                              <option value="2" <?php if(isset($row_e['status'])) echo ($row_e['status'] == "2")?"selected":"";?>>Disable</option>

                                            </select>

                                    </div>
                                  </div>

                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><?=isset($row_e) ? 'UPDATE' : 'CREATE'?></button>
                            </div>
                          </form>
                        </div>
                      </div>


                      <div class="card">
                          <div class="card-body">

                              <div class="table-responsive">
                                  <table class="table table-striped">
                                      <thead>
                                          <tr>
                                              <th>Sort Order</th>
                                              <th>Image</th>
                                              <th>Alt</th>
                                              <th>Content 1</th>
                                              <th>Content 2</th>
                                              <th>Status</th>
                                              <th class="text-nowrap">Action</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php

                                      $result = $obj->select_all_by_sort_order("product_slider");
                                      while($row= mysqli_fetch_assoc($result)){
                                      ?>
                                          <tr>
                                              <td><?=$row['sort_order']?></td>
                                              <td><img src="../img/slider/<?=$row['banner_image']?>"  width="150px"></td>
                                              <td><?=$row['alt']?></td>
                                              <td><?=stripslashes($row['content1'])?></td>
                                              <td><?=stripslashes($row['content2'])?></td>
                                              <td style="color:<?php echo ($row['status']=="1")?"green":"red";?>"><?php echo ($row['status']=="1")?"Active":"Delete";?></td>
                                              <td class="text-nowrap">
                                                  <a href="?edit=<?=$row['id']?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                  <a href="?delete=<?=$row['id']?>" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                              </td>
                                          </tr>
                                          <?
                                         }
                                        ?>

                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>

                </div>



            </div>



            <? include("includes/footer.php"); ?>

        </div>

    </div>

    <?php include("includes/foot.php"); ?>

    <script>
          <? if(isset($_REQUEST['edit'])){ ?>
              $(function() {
                    $("#add_slider").toggle();
              });
          <? } ?>
      </script>
      <script>
      $(document).ready(function(){
        $("#add").click(function(){
          $("#add_slider").toggle();
        });
      });
      </script>



</body>

</html>
