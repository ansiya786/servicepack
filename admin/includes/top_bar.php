<?php
$user = $obj->get_user('tbl_user');
?>
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="<?=$obj->admin_url?>dashboard.php">
                <!-- Logo icon --><b>
                
                    <img src="<?=$obj->base_url?>img/footer-logo.svg" alt="homepage" class="dark-logo" style="width: 70%;" />
                    <!-- Light Logo icon -->
                    <img src="<?=$obj->base_url?>img/footer-logo.svg" alt="homepage" class="light-logo" style="width: 70%;" />
                </b>
                <!--End Logo icon -->
                <!-- Logo text --><span>
        </div>

        <div class="navbar-collapse">

            <ul class="navbar-nav mr-auto mt-md-0">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>

            </ul>

            <ul class="navbar-nav my-lg-0">


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?=$obj->admin_url?>assets/images/users/default-profile.png" alt="user" class="profile-pic" /></a>
                    <div class="dropdown-menu dropdown-menu-right scale-up">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-text">
                                        <h4 style="color: white"><?php echo $user['name']; ?></h4>
                                        <p class="text-muted"><?php echo $user['email']; ?></p></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=$obj->admin_url?>logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
