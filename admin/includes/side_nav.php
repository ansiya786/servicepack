<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img">
                <!-- <img src="../assets/images/users/profile.png" alt="user" /> -->
                <!-- this is blinking heartbit-->
                <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
            </div>
            <!-- User profile text-->
            <div class="profile-text">
                <h5>
                    <?php echo $user['name']; ?>
                </h5>

                <a href="logout.php" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
                <div class="dropdown-menu animated flipInY">

                    <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a>

                    <a href="logout.php" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>

                </div>
            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <!-- <li> <a class="waves-effect waves-dark" href="dashboard.php" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a></li> -->
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                            class="mdi mdi-home"></i><span class="hide-menu">Home</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="product_slider.php">Slider Management</a></li>
                        <li><a href="brandlogo.php">Brand Logo Management</a></li>
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                            class="mdi mdi-map"></i><span class="hide-menu">Product</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="brand_management.php">Brand Management</a></li>
                        <li><a href="model_management.php">Model Management</a></li>
                        <li><a href="series_management.php">Series Management</a></li>
                        <li><a href="color_management.php">Color Management</a></li>
                        <li><a href="product_management.php">Product Management</a></li>
                    </ul>
                </li>

                <li> <a class="waves-effect waves-dark" href="newsletter_email.php" aria-expanded="false"><i
                            class="mdi  mdi-email"></i><span class="hide-menu">Newsletter Email</span></a></li>
                <li> <a class="waves-effect waves-dark" href="enquiry.php" aria-expanded="false"><i
                            class="mdi  mdi-message-text"></i><span class="hide-menu">Enquiry</span></a></li>


            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>