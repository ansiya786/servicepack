    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=$obj->base_url?>img/ashtel-logo.svg">
    <link href="<?=$obj->admin_url?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$obj->admin_url?>css/style.css" rel="stylesheet">
    <link href="<?=$obj->admin_url?>css/colors/blue-dark.css" id="theme" rel="stylesheet">

    <link href="<?=$obj->admin_url?>assets/plugins/morrisjs/morris.css" rel="stylesheet">

