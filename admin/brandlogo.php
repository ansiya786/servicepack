<?php
include ("db/connection.php");
$obj=new servicepack();
$obj->RestrictAccess();

if(isset($_POST['status']))
{

  $data['status']=$_POST['status'];
  $data['logo_image'] =  $_POST['hiddenimg'];
  if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {
   $data['logo_image'] = $obj->upload_file_attach("../img/logo/", $_FILES["image"]);
  }

  if(isset($_POST['id'])) {
    $row_e = $obj->select_by_id("brand_logo",$_POST['id']);
    if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {
       $obj->unlinkFile( '../img/logo/' . $row_e['logo_image']);
    }
    $obj->update_data("brand_logo",$data, $_POST['id']);
   }else {
    $obj->insert_data("brand_logo",$data);
   }

    header("location:brandlogo.php");
}
if(isset($_REQUEST['edit'])){
    $row_e = $obj->select_by_id("brand_logo",$_REQUEST['edit']);
}
if(isset($_REQUEST['delete'])){
  $row_e = $obj->select_by_id("brand_logo",$_REQUEST['delete']);
  if(!empty($row_e['logo_image'])) {
   $obj->unlinkFile( '../img/logo/' . $row_e['logo_image']);
  }
  $r_d = $obj->delete_by_id("brand_logo",$_REQUEST['delete']);
  header("location:brandlogo.php");
}

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <?php include("includes/head.php"); ?>
    <title>Product Slider | Admin Dashboard</title>

</head>

<body class="fix-header card-no-border">

    <div id="main-wrapper">

        <?php include ("includes/top_bar.php"); ?>
        <?php include ("includes/side_nav.php"); ?>

        <div class="page-wrapper">

        <div class="row page-titles"></div>

            <div class="container-fluid">
              <div class="row">
              <div class="col-md-4">
                          <div class="button-box">
                            <button type="button" class="btn btn-success" id="add"><?=isset($row_e) ? 'EDIT' : 'ADD'?> </button>
                          </div>

              </div>
            </div>
                <div class="row">

                  <div class="col-12">
                      <div class="card" id="add_slider" style="display:none">
                          <div class="card-body">
                            <h4 class="card-title"><?=isset($row_e) ? 'EDIT' : 'ADD'?> </h4>
                            <form action="" method="post" enctype='multipart/form-data'>
                            <div class="modal-body">
                              <?  if(isset($row_e)){ ?>
                                      <input type="hidden" name="id" value="<?=$row_e['id']?>">
                                  <? } ?>

                                    <div class="form-row">

                                    <div class="col-md-4 mb-3">
                                        <label for="image" class="control-label">Banner Image</label>
                                        <input type="file" class="form-control" id="image" name="image" accept="image/x-png,image/jpeg,image/jpg">
                                        <input type="hidden" class="form-control" name="hiddenimg" value="<?=isset($row_e) ? $row_e['logo_image'] : ''?>">
                                    </div>
                                
                                    <div class="col-md-4 mb-3">
                                        <label for="status" class="control-label">Status</label>

                                            <select class="form-control custom-select" data-placeholder="Choose Status" tabindex="1" name="status" required>
                                              <option value="1" <?php if(isset($row_e['status'])) echo ($row_e['status'] == "1")?"selected":"";?>>Enable</option>
                                              <option value="2" <?php if(isset($row_e['status'])) echo ($row_e['status'] == "2")?"selected":"";?>>Disable</option>

                                            </select>

                                    </div>
                                  </div>

                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><?=isset($row_e) ? 'UPDATE' : 'CREATE'?></button>
                            </div>
                          </form>
                        </div>
                      </div>


                      <div class="card">
                          <div class="card-body">

                              <div class="table-responsive">
                                  <table class="table table-striped">
                                      <thead>
                                          <tr>
                                              <th>Sl No</th>
                                              <th>Image</th>
                                              <th>Status</th>
                                              <th class="text-nowrap">Action</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                      $i=1;
                                      $result = $obj->select_all("brand_logo");
                                      while($row= mysqli_fetch_assoc($result)){
                                      ?>
                                          <tr>
                                              <td><?php echo $i++; ?></td>
                                              <td><img src="../img/logo/<?=$row['logo_image']?>"  width="150px"></td>
                                              <td style="color:<?php echo ($row['status']=="1")?"green":"red";?>"><?php echo ($row['status']=="1")?"Active":"Delete";?></td>
                                              <td class="text-nowrap">
                                                  <a href="?edit=<?=$row['id']?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                  <a href="?delete=<?=$row['id']?>" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                              </td>
                                          </tr>
                                          <?
                                         }
                                        ?>

                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>

                </div>



            </div>



            <? include("includes/footer.php"); ?>

        </div>

    </div>

    <?php include("includes/foot.php"); ?>

    <script>
          <? if(isset($_REQUEST['edit'])){ ?>
              $(function() {
                    $("#add_slider").toggle();
              });
          <? } ?>
      </script>
      <script>
      $(document).ready(function(){
        $("#add").click(function(){
          $("#add_slider").toggle();
        });
      });
      </script>



</body>

</html>
