<?php
include ("db/connection.php");
$obj=new servicepack();
$obj->RestrictAccess();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("includes/head.php"); ?>
  <title>Enquiry | Admin Dashboard</title>
  
</head>
<body class="fix-header card-no-border">
<!-- <div class="preloader">
  <svg class="circular" viewBox="25 25 50 50">
  <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div> -->

<div id="main-wrapper">
<?php include ("includes/top_bar.php"); ?>

<?php include ("includes/side_nav.php"); ?>
  <div class="page-wrapper">
    <div class="row page-titles">
    </div>
    <div class="container-fluid">
    

        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Enquiry</h4>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Enq No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Created Date</th>
                    <th class="text-nowrap">Action</th>
                    </tr>
                  </thead>
                    <tbody>
                      <?php
                        $i=0;
                        $resultfetch = $obj->select_all_by_desc("tbl_enquiry");
                        while($rowfetch= mysqli_fetch_assoc($resultfetch)){
                      ?>
                      <tr>
                        <td><?php echo $i=$i+1;?></td>
                        <td><?php echo ucwords($rowfetch['enq_no']);?></td>
                        <td><?php echo ucwords($rowfetch['name']);?></td>
                        <td><?php echo ucwords($rowfetch['email']);?></td>
                        <td><?=stripslashes($rowfetch['phone'])?></td>
                        <td><?=date('Y-m-d', strtotime( $rowfetch['created_at'] ) );?></td>                       
                        <td><button id="myBtn" class="btn waves-effect waves-light btn-rounded  btn-info btn-lg" onclick="viewProductItems(<?=$rowfetch['id']; ?>)" data-toggle="modal<?php echo $rowfetch['id']?>" data-target="#myModal">View</button></td>
                     
                      </tr>
                      <?php
                        }
                      ?>
                    </tbody>
                </table>
              </div>
            </div>



          </div>
        </div>
      </div>

     
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Enquiry Items</h4>

                    </div>
                    <div class="modal-body">

                    <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table color-table dark-table">
                                        <thead>
                                            <tr>
                                                <th>Sl.No</th>
                                                <th>Product name</th>
                                                <th>Brand</th>
                                                <th>Model</th>
                                                <th>Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody id="txtdata">
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                </div>
            </div>
        </div>

      <?php include("includes/footer.php"); ?>
     </div>
    </div>
  </div>
</body>
<? include("includes/foot.php"); ?>
<script>

function viewProductItems(id)
{
  var enq_id = id;
  $.ajax({
    url: "viewProductItemsbyEnquiry",
    type: "POST",
    data: {
      enq_id:enq_id,

    },
    cache: false,
    success: function(dataResult){

      $('#myModal').modal('show'); 
      $("#txtdata").html(dataResult);

    }
  });

}
    </script>
   
   

</html>
