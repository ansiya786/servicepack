<?php
include("db/connection.php");
$obj=new servicepack();
if (isset($_POST["Import"])) {


     $filename = $_FILES["file"]["tmp_name"];


	if ($_FILES["file"]["size"] > 0) {


		$file = fopen($filename, "r");
		while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE) {


			$brand_name    		= $emapData[0];
			$model_name    		= $emapData[1];
			$focus_name         = $emapData[2];
			$description   		= $emapData[3];
			$display_name  		= $emapData[4];
			$common_keywords  	= $emapData[5];
			$specific_keywords  = $emapData[6];
			$wrongtype_keywords = $emapData[7];
			$status        		= 1;

			$where = "name='$brand_name'";
			$brand_id = $obj->check_exists("tbl_brand", $where);
			
			$data_brand['name'] = $brand_name;
			$data_model['name'] = $model_name;
            if ($brand_id == 0 && $brand_name !="") {
		      $brand_id = $obj->insert_data("tbl_brand", $data_brand);
			}
			$where = "brand_id = $brand_id and name='$model_name'";
			$model_id = $obj->check_exists("tbrl_brand_model", $where);
			if ($model_id == 0 && $model_name !="") {
			    $data_model['brand_id'] = $brand_id;
			    $model_id = $obj->insert_data("tbrl_brand_model", $data_model);
			}
			$where = "brand_id = $brand_id and model_id = $model_id and focus_name = '$focus_name'";
			$product_id = $obj->check_exists("tbl_product", $where);
			if ($product_id == 0 && $focus_name !="") {
			    $data_product['brand_id']    	 	= $brand_id;
				$data_product['model_id']   	 	= $model_id;
				$data_product['focus_name']      	= $focus_name;
				$data_product['description'] 	 	= $description;
				$data_product['display_name']    	= $display_name;
				$data_product['common_keywords']    = $common_keywords;
				$data_product['specific_keywords']  = $specific_keywords;
				$data_product['wrongtype_keywords'] = $wrongtype_keywords;
				$data_product['status']    	     	= $status;
				$product_id = $obj->insert_data("tbl_product", $data_product);   
			}
		}

	
		fclose($file);
		echo "<script type=\"text/javascript\">
					window.location = \"product_management.php\"
			  </script>";

	}
}
