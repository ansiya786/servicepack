<?php
include("admin/db/connection.php");
$obj = new servicepack();
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>Service Pack | Home</title>
    <?php include("includes/head.php"); ?>
</head>
<style>
 
</style>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">

        <!-- Header -->
        <?php include("includes/header.php"); ?>


        <!-- Slid Sec -->
        <section class="slid-sec">
            <div class="">
                <div class="container-fluid">
                    <div class="row">

                        <!-- Main Slider  -->
                        <div class="col-md-12 no-padding">

                            <!-- Main Slider Start -->
                            <div class="tp-banner-container">
                                <div class="tp-banner">
                                    <ul>
                                        <?php
                                        $result = $obj->select_all_active('product_slider');
                                        while ($row = mysqli_fetch_assoc($result)) {

                                        ?>
                                            <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                                                <img src="<?= $obj->base_url ?>img/slider/<?= $row['banner_image'] ?>" alt="slider" data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat">

                                                <div class="tp-caption sfl tp-resizeme white-color" data-x="center" data-hoffset="60" data-y="center" data-voffset="-110" data-speed="800" data-start="800" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="none" data-elementdelay="0.03" data-endelementdelay="0.4" data-endspeed="300" style="z-index: 5; font-size:40px; font-weight:800; color:#888888;  max-width: auto; max-height: auto; white-space: nowrap;">
                                                    For your LCD Display necessities,</div>

                                                <div class="tp-caption sfr tp-resizeme white-color" data-x="center" data-hoffset="60" data-y="center" data-voffset="-60" data-speed="800" data-start="1000" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="none" data-elementdelay="0.03" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 6; font-size:40px; color:#0088cc; font-weight:500; white-space: nowrap;">
                                                    We can serve you better.</div>

                                                <div class="tp-caption lfb tp-resizeme scroll" data-x="center" data-hoffset="60" data-y="center" data-voffset="30" data-speed="800" data-start="1300" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-scrolloffset="0" style="z-index: 8;"><a href="#." class="btn-round big">Bulk Buying!</a>
                                                </div>
                                            </li>
                                        <?php
                                        }
                                        ?>
                                        <!-- <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                                            <img src="<?= $obj->base_url ?>img/slider/img.png" alt="slider" data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat">

                                            <div class="tp-caption sfl tp-resizeme white-color" data-x="center" data-hoffset="60" data-y="center" data-voffset="-110" data-speed="800" data-start="800" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="none" data-elementdelay="0.03" data-endelementdelay="0.4" data-endspeed="300" style="z-index: 5; font-size:40px; font-weight:800; color:#888888;  max-width: auto; max-height: auto; white-space: nowrap;">
                                                For your LCD Display necessities,</div>

                                            <div class="tp-caption sfr tp-resizeme white-color" data-x="center" data-hoffset="60" data-y="center" data-voffset="-60" data-speed="800" data-start="1000" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="none" data-elementdelay="0.03" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 6; font-size:40px; color:#0088cc; font-weight:500; white-space: nowrap;">
                                                We can serve you better.</div>

                                            <div class="tp-caption lfb tp-resizeme scroll" data-x="center" data-hoffset="60" data-y="center" data-voffset="30" data-speed="800" data-start="1300" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-scrolloffset="0" style="z-index: 8;"><a href="#." class="btn-round big">Bulk Buying!</a>
                                            </div>
                                        </li>
                                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                                            <img src="<?= $obj->base_url ?>img/slider/img.png" alt="slider" data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat">

                                            <div class="tp-caption sfl tp-resizeme white-color" data-x="center" data-hoffset="60" data-y="center" data-voffset="-110" data-speed="800" data-start="800" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="none" data-elementdelay="0.03" data-endelementdelay="0.4" data-endspeed="300" style="z-index: 5; font-size:40px; font-weight:800; color:#888888;  max-width: auto; max-height: auto; white-space: nowrap;">
                                                For your LCD Display necessities,</div>

                                            <div class="tp-caption sfr tp-resizeme white-color" data-x="center" data-hoffset="60" data-y="center" data-voffset="-60" data-speed="800" data-start="1000" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="none" data-elementdelay="0.03" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 6; font-size:40px; color:#0088cc; font-weight:500; white-space: nowrap;">
                                                We can serve you better.</div>

                                            <div class="tp-caption lfb tp-resizeme scroll" data-x="center" data-hoffset="60" data-y="center" data-voffset="30" data-speed="800" data-start="1300" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-scrolloffset="0" style="z-index: 8;"><a href="#." class="btn-round big">Bulk Buying!</a>
                                            </div>
                                        </li> -->

                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </section>

        <section class="padding-top-30 padding-bottom-0 mbt-0">
            <div class="container">

                <!-- heading -->
                <div class="center-heading text-center">
                    <h2 class="main-heading">
                        <p>Why You Should Choose</p><span>Service Pack Online?..</span>
                    </h2>
                    <hr class="about-head-line">
                </div>
                <p class="shot-desc">With a focus on quality, efficient bulk order handling, and exceptional customer
                    service,
                    Service pack Online has become a reliable partner for businesses and repair professionals worldwide.
                </p>
                <div class="about-service margin-top-30">
                    <div class="col-md-6 about-service-two">
                        <div class="blog-post">
                            <img class="img-responsive" src="<?= $obj->base_url ?>img/about/about-Group1.jpg" alt="">
                            <p class="text-center"><b>Bulk Order Handling</b></p>
                            <p class="text-center">Handling bulk orders is a specialty of Service Pack Online Company,
                                which serves merchants,
                                enterprises, and repair facilities with significant needs. The business has the systems
                                in place
                                and the logistics expertise to effectively handle and fulfill large orders, providing
                                prompt
                                and affordable supply to its clients.</p>
                        </div>
                    </div>
                    <div class="col-md-6 about-service-two">
                        <div class="blog-post">
                            <img class="img-responsive" src="<?= $obj->base_url ?>img/about/about-Group2.jpg" alt="">
                            <p class="text-center"><b>Quality Assurance</b></p>
                            <p class="text-center">The Service Pack Online Company is committed to giving its clients
                                high-quality items. Authenticity and dependability are ensured by the company's direct
                                sourcing of spare parts and accessories from manufacturers and authorized distributors.
                                To uphold the company's dedication to excellence, stringent quality control procedures
                                are used to confirm the reliability and effectiveness of every product.</p>
                        </div>
                    </div>
                    <div class="justify-content-center about-service-one">
                        <div class="blog-post">
                            <img class="img-responsive" src="<?= $obj->base_url ?>img/about/about-Group3.jpg" alt="">
                            <p class="text-center"><b>Customer Support</b></p>
                            <p class="text-center">Service Pack Online Company is well known for providing excellent
                                customer service. The business has a committed group of qualified experts that are
                                committed to helping clients with the ordering process, answering questions, and
                                offering professional advice as needed. The company is a dependable partner for
                                companies in the mobile phone sector because to prompt responses, effective order
                                administration, and a focus on customers. </p>
                        </div>
                    </div>
                </div>
                <div class="btn-center padding-top-30">
                    <a href="<?= $obj->base_url ?>about" class="btn-round">Learnmore...</a>
                </div>

            </div>
        </section>

        <!-- Content -->
        <div id="content">
            <!-- Main Tabs Sec -->
            <section class="main-tabs-sec padding-top-60 padding-bottom-0">
                <div class="container">
                    <div class="center-heading text-center">
                        <h2 class="main-heading">
                            <p>LATEST PRODUCTS</p><span>Service Pack Lcd.</span>
                        </h2>
                        <hr class="latest-head-line">
                    </div>

                    <!-- Tab panes -->
                    <div class="tab-content padding-top-40">
                        <!-- TV & Audios -->
                        <div role="tabpanel" class="tab-pane active fade in" id="tv-au">

                            <!-- Items -->
                            <div class="item-slide-4 with-bullet no-nav">
                                <!-- Product -->
                                <?php
                                $result = $obj->getLatestProducts();
                                while ($row = mysqli_fetch_assoc($result)) {
                                ?>
                                    <div class="product">
                                        <article>
                                            <a href="product-details?id=<?= $row['id'] ?>"><img class="img-responsive" src="<?= $obj->base_url ?>admin/uploads/images/<?= $row['image'] ?>" alt=""> <img class="sale-tag" src="<?= $obj->base_url ?>img/servicepack.png"></a>
                                            <div class="product-name">
                                                <!-- <span class="tag"><a href="product-details?id=<?= $row['id'] ?>"><?= $row['brand_name'] ?> </a></span> <a href="product-details?id=<?= $row['productid'] ?>" class="title"><?= $row['productname'] ?></a> -->
                                                <a href="product-details?id=<?= $row['id'] ?>"><span class="tag"><?= $row['brand_name'] ?> <?= $row['series_name'] ?>.</a></span> <span class="tittle"><a href="product-details?id=<?= $row['id'] ?>">Samsung Galaxy A33 5G 2022 (SM-A336B) Lcd Display Unit
                                                        Complete White +Btry GH82-28145B</a></span>
                                                </a>
                                            </div>
                                            <div class="product-btns">
                                                <div class="price" onclick="return addtocart(<?= $row['id'] ?>,'add')"><img src="<?= $obj->base_url ?>img/shopping-bag.png">Add to Cart</div>
                                                <div class="cart-ico">
                                                    <a href="https://api.whatsapp.com/send?phone=+971557802100&text=Hi, I am interested about <?= $row['brand_name'] ?> <?= $row['model_name'] ?> <?= $row['series_name'] ?> <?= $row['part_number'] ?>  LCD <?= $row['color'] ?>. Please let me know details" target="_blank" class="share-btn">
                                                        <img src="<?= $obj->base_url ?>img/whatsapp.png">Quick Enquiry
                                                    </a>
                                                </div>
                                            </div>

                                        </article>
                                    </div>

                                <?php
                                }
                                ?>
                                <!-- <div class="product">
                                    <article> <img class="img-responsive" src="img/products/Image.jpg" alt=""> <img class="sale-tag" src="img/servicepack.png">
                                        <div class="product-name">
                                            <span class="tag">Samsung Galaxy A series.</span> <a href="#." class="tittle">Samsung Galaxy A33 5G 2022 (SM-A336B) Lcd Display Unit
                                                Complete White +Btry GH82-28145B</a>
                                        </div>
                                        <div class="product-btns">
                                            <div class="price"><img src="img/shopping-bag.png">Add to Cart</div>
                                            <div class="cart-ico"><img src="img/whatsapp.png">Quick Enquiry</div>
                                        </div>

                                    </article>
                                </div>
                                <div class="product">
                                    <article> <img class="img-responsive" src="<?= $obj->base_url ?>img/products/Image.jpg" alt=""> <img class="sale-tag" src="<?= $obj->base_url ?>img/servicepack.png">
                                        <div class="product-name">
                                            <span class="tag">Samsung Galaxy A series.</span> <a href="#." class="tittle">Samsung Galaxy A33 5G 2022 (SM-A336B) Lcd Display Unit
                                                Complete White +Btry GH82-28145B</a>
                                        </div>
                                        <div class="product-btns">
                                            <div class="price"><img src="<?= $obj->base_url ?>img/shopping-bag.png">Add to Cart</div>
                                            <div class="cart-ico"><img src="<?= $obj->base_url ?>img/whatsapp.png">Quick Enquiry</div>
                                        </div>

                                    </article>
                                </div>
                                <div class="product">
                                    <article> <img class="img-responsive" src="<?= $obj->base_url ?>img/products/Image.jpg" alt=""> <img class="sale-tag" src="<?= $obj->base_url ?>img/servicepack.png">
                                        <div class="product-name">
                                            <span class="tag">Samsung Galaxy A series.</span> <a href="#." class="tittle">Samsung Galaxy A33 5G 2022 (SM-A336B) Lcd Display Unit
                                                Complete White +Btry GH82-28145B</a>
                                        </div>
                                        <div class="product-btns">
                                            <div class="price"><img src="<?= $obj->base_url ?>img/shopping-bag.png">Add to Cart</div>
                                            <div class="cart-ico"><img src="<?= $obj->base_url ?>img/whatsapp.png">Quick Enquiry</div>
                                        </div>

                                    </article>
                                </div>
                                <div class="product">
                                    <article> <img class="img-responsive" src="<?= $obj->base_url ?>img/products/Image.jpg" alt=""> <img class="sale-tag" src="<?= $obj->base_url ?>img/servicepack.png">
                                        <div class="product-name">
                                            <span class="tag">Samsung Galaxy A series.</span> <a href="#." class="tittle">Samsung Galaxy A33 5G 2022 (SM-A336B) Lcd Display Unit
                                                Complete White +Btry GH82-28145B</a>
                                        </div>
                                        <div class="product-btns">
                                            <div class="price"><img src="<?= $obj->base_url ?>img/shopping-bag.png">Add to Cart</div>
                                            <div class="cart-ico"><img src="<?= $obj->base_url ?>img/whatsapp.png">Quick Enquiry</div>
                                        </div>

                                    </article>
                                </div>
                                <div class="product">
                                    <article> <img class="img-responsive" src="<?= $obj->base_url ?>img/products/Image.jpg" alt=""> <img class="sale-tag" src="<?= $obj->base_url ?>img/servicepack.png">
                                        <div class="product-name">
                                            <span class="tag">Samsung Galaxy A series.</span> <a href="#." class="tittle">Samsung Galaxy A33 5G 2022 (SM-A336B) Lcd Display Unit
                                                Complete White +Btry GH82-28145B</a>
                                        </div>
                                        <div class="product-btns">
                                            <div class="price"><img src="<?= $obj->base_url ?>img/shopping-bag.png">Add to Cart</div>
                                            <div class="cart-ico"><img src="<?= $obj->base_url ?>img/whatsapp.png">Quick Enquiry</div>
                                        </div>

                                    </article>
                                </div> -->
                            </div>

                        </div>
                        <div class="btn-center padding-top-30">
                            <a href="<?= $obj->base_url ?>products" class="btn-round">View All Products</a>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Top Selling Week -->
            <section class="padding-top-60 padding-bottom-60">
                <div class="container">

                    <!-- heading -->
                    <div class="center-heading text-center">
                        <h2 class="main-heading">
                            <p>Customers</p><span>Feedback.</span>
                        </h2>
                        <hr class="customer-head-line">
                    </div>

                    <div class="item-slide-3 with-bullet no-nav  margin-top-50">
                        <!-- Product -->
                        <div class="testimonial">
                            <article>
                                <div class="testimonial-content">
                                    <p>I am extremely satisfied with the Service Pack OLED display's product quality. It has truly elevated my viewing experience, making everything on the screen more captivating and immersive.</p>

                                </div>
                                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </p>
                                <div class="tag">John travolta</span>

                            </article>

                        </div>
                        <div class="testimonial">
                            <article>
                                <div class="testimonial-content">
                                    <p>I am extremely satisfied with the Service Pack OLED display's product quality. It has truly elevated my viewing experience, making everything on the screen more captivating and immersive.</p>

                                </div>
                                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </p>
                                <div class="tag">John travolta</span>

                            </article>

                        </div>
                        <div class="testimonial">
                            <article>
                                <div class="testimonial-content">
                                    <p>I am extremely satisfied with the Service Pack OLED display's product quality. It has truly elevated my viewing experience, making everything on the screen more captivating and immersive.</p>

                                </div>
                                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </p>
                                <div class="tag">John travolta</span>

                            </article>

                        </div>
                        <div class="testimonial">
                            <article>
                                <div class="testimonial-content">
                                    <p>I am extremely satisfied with the Service Pack OLED display's product quality. It has truly elevated my viewing experience, making everything on the screen more captivating and immersive.</p>

                                </div>
                                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </p>
                                <div class="tag">John travolta</span>

                            </article>

                        </div>
                        <div class="testimonial">
                            <article>
                                <div class="testimonial-content">
                                    <p>I am extremely satisfied with the Service Pack OLED display's product quality. It has truly elevated my viewing experience, making everything on the screen more captivating and immersive.</p>

                                </div>
                                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </p>
                                <div class="tag">John travolta</span>

                            </article>

                        </div>
                        <div class="testimonial">
                            <article>
                                <div class="testimonial-content">
                                    <p>I am extremely satisfied with the Service Pack OLED display's product quality. It has truly elevated my viewing experience, making everything on the screen more captivating and immersive.</p>

                                </div>
                                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </p>
                                <div class="tag">John travolta</span>

                            </article>

                        </div>
                    </div>
                </div>
            </section>

            <!-- Clients img -->
            <section class="clients-img padding-top-0 padding-bottom-60">
                <div class="container">
                    <div class="item-slide-6 ">
                        <!-- Product -->
                        <div>
                            <article> <img src="<?= $obj->base_url ?>img/logo/samsung.png" alt="">

                            </article>
                        </div>
                        <div>
                            <article> <img src="<?= $obj->base_url ?>img/logo/samsung.png" alt="">

                            </article>
                        </div>

                        <div>
                            <article> <img src="<?= $obj->base_url ?>img/logo/samsung.png" alt="">

                            </article>
                        </div>
                        <div>
                            <article> <img src="<?= $obj->base_url ?>img/logo/samsung.png" alt="">

                            </article>
                        </div>

                        <div>
                            <article> <img src="<?= $obj->base_url ?>img/logo/samsung.png" alt="">

                            </article>
                        </div>
                        <div>
                            <article> <img src="<?= $obj->base_url ?>img/logo/samsung.png" alt="">

                            </article>
                        </div>

                        <div>
                            <article> <img src="<?= $obj->base_url ?>img/logo/samsung.png" alt="">

                            </article>
                        </div>
                        <div>
                            <article> <img src="<?= $obj->base_url ?>img/logo/samsung.png" alt="">

                            </article>
                        </div>
                    </div>
                </div>
            </section>

        </div>

        <?php include("includes/footer.php"); ?>


    </div>


    <?php include("includes/foot.php"); ?>

</html>