<?php
include("admin/db/connection.php");
$obj = new servicepack();
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>Service Pack | Home</title>
    <?php include("includes/head.php"); ?>


</head>
<STYLE>
 .glyphicon { margin-right:5px; }
.thumbnail
{
    margin-bottom: 20px;
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}

.item.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
    background: #428bca;
}

.item.list-group-item .list-group-image
{
    margin-right: 10px;
}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
}

.item.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}



.item.list-group-item img
{
    float: left;
}
.item.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    margin: 0 0 11px;
}

</STYLE>
<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">

        <!-- Header -->
        <?php include("includes/header.php"); ?>


        <section class="padding-top-40 padding-bottom-60">
        <div class="container">

    <div class="short-lst">
                       
                       <ul>
                           <!-- Short List -->
                           
                           <!-- Short  -->
                           
                           <!-- by Default -->
                           
                           
                           <!-- Grid Layer -->
                           <li class="grid-layer"> <a href="#." id="list"><i class="fa fa-list margin-right-10"></i></a> <a href="#." id="grid"><i class="fa fa-th"></i></a> </li>
                       </ul>
                   </div>




    <div id="products" class="row list-group">
        <div class="item  col-xs-4 col-lg-4">
            <div class="product">
            <article> <img class="img-responsive" src="img/products/Image.jpg" alt=""> <img class="sale-tag" src="img/servicepack.png">
                                    <div class="product-name">
                                        <span class="tag">Samsung Galaxy A series.</span> <a href="#." class="tittle">Samsung Galaxy A33 5G 2022 (SM-A336B) Lcd Display Unit
                                            Complete White +Btry GH82-28145B</a>
                                    </div>
                                    <div class="product-btns">
                                        <div class="price"><img src="img/shopping-bag.png">Add to Cart</div>
                                        <div class="cart-ico"><img src="img/whatsapp.png">Quick Enquiry</div>
                                    </div>

                                </article>
            </div>
        </div>
        <div class="item  col-xs-4 col-lg-4">
            <div class="thumbnail">
            <article> <img class="img-responsive" src="img/products/Image.jpg" alt=""> <img class="sale-tag" src="img/servicepack.png">
                                    <div class="product-name">
                                        <span class="tag">Samsung Galaxy A series.</span> <a href="#." class="tittle">Samsung Galaxy A33 5G 2022 (SM-A336B) Lcd Display Unit
                                            Complete White +Btry GH82-28145B</a>
                                    </div>
                                    <div class="product-btns">
                                        <div class="price"><img src="img/shopping-bag.png">Add to Cart</div>
                                        <div class="cart-ico"><img src="img/whatsapp.png">Quick Enquiry</div>
                                    </div>

                                </article>
            </div>
        </div>

    </div>
</div>

        </section>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script>
   $(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');
    $('#products .item').addClass('grid-group-item');});
});
  </script>
        <?php include("includes/footer.php"); ?>


    </div>


    <?php include("includes/foot.php"); ?>

</html>