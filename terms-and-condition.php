<?php
include("admin/db/connection.php");
$obj = new servicepack();
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Service Pack | About Us</title>
    <?php include("includes/head.php"); ?>
</head>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">

        <!-- Header -->
        <?php include("includes/header.php"); ?>

        <section>
            <div class="aboutus-banner"
                style="background:linear-gradient(0deg, #222222, rgba(255,255,255,.15)), url(img/about/aboutus.jpg) no-repeat">
                <div class="container text-center position-center-center">
                    <h1>Terms & Conditions</h1>
                    <h2>Service Pack LCD</h2>
                </div>
            </div>
        </section>
        <!-- Content -->

        <section class="padding-top-80 padding-bottom-80">
            <div class="container">


                <!-- heading -->
                <div class="heading">
                    <h2>Terms and Conditions </h2>

                </div>
                <p>These terms and conditions govern the use of Servicepack Online Company's website and the purchase of
                    products offered by Servicepack Online Company.
                    Your use of the Website is subject to these Terms & Conditions. You certify that you have read,
                    comprehended, and agree to all of these Terms & Conditions by accessing,
                    using, or registering on our Website. By using our website and purchasing our products, you agree to
                    be bound by these terms and conditions. Please read them carefully
                    before making a purchase. Any new conditions will take effect immediately after being posted on our
                    website.
                </p>
                <p>Please don't use this site if you don't agree to these terms and conditions. Parts of this policy may
                    be changed, modified, or removed at any time, in our sole discretion.
                    The company may occasionally change its privacy notice. By posting a notice on our website, we'll
                    let you know if there are any significant changes in the way we treat personal
                    information. To ensure that you always are aware of the data we gather, how we use it, and who we
                    disclose it to, we invite you to check back and review our policy on a regular basis. </p>
                <div class="heading">
                    <h2>Usage License</h2>

                </div>
                <p>The Servicepack online website's materials (information or software) may be downloaded with no
                    restrictions. This license only grants you access to the materials; it does not transfer
                    ownership of the materials to you. Use the materials for any commercial or public exhibition,
                    whether it be for profit or not; attempting to reverse-engineer or decompile any software
                    from the website; the materials should be free of any copyright or other proprietary markings; or
                    You may give the materials to someone else or "mirror" them on another server.
                    If you breach any of these restrictions, your license to use the website or its content will
                    automatically expire. You are required to delete any downloaded materials in your possession,
                    whether they be in electronic or printed format, after viewing the contents or after this license is
                    terminated.
                </p>
                <div class="heading">
                    <h2>Prohibited Uses</h2>

                </div>
                <p>You are forbidden from using the website or its content, in addition to other restrictions outlined
                    in the Terms of Service: Infringing upon or violating our intellectual property rights or
                    the intellectual property rights of others includes: (a) for any unlawful purpose; (b) to solicit
                    others to perform or participate in any unlawful acts; (c) to violate any international, federal,
                    provincial, or state regulations, rules, laws, or local ordinances; (d) to harass, abuse, insult,
                    harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual
                    orientation, religion, ethnicity, or f) to provide false or misleading information; g) to upload or
                    transmit viruses or any other type of malicious code that will or may be used to
                    interfere with or circumvent the security features of the Service or of any related website, other
                    websites, or the Internet; h) to collect or track the personal information of others; )
                    to spam, phish, pharm, pretext, spider, crawl, or scrape; j) for any obscene or immoral purpose; or
                    k) For breaching any of the banned uses, we retain the right to stop you from
                    using the Service or any connected website.
                </p>

            </div>
        </section>

        <!-- End Content -->

        <!-- Footer -->
        <?php include("includes/footer.php"); ?>
       
    </div>
    <!-- End Page Wrapper -->
    <?php include("includes/foot.php"); ?>

</html>