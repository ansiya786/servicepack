<?php
include("admin/db/connection.php");
$obj = new servicepack();
$product_id = $_GET["id"];

$product = $obj->select_all_products_by_id('tbrl_products',$product_id);


$related_products = $obj->getProductsBySeriesId($product['series_id']); 
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>Service Pack | Brand </title>
    <?php include("includes/head.php"); ?>
</head>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">

        <!-- Header -->
        <?php include("includes/header.php"); ?>
        <div class="product-header">
            <div class="container">
                <div class="text-center padding-top-20 padding-bottom-20">
                    <p><?php echo $product['name']; ?></p>
                </div>
            </div>
        </div>
        <!-- Content -->

        <!-- Content -->
        <div id="content">

            <!-- Products -->
            <section class="padding-top-40 padding-bottom-40">
                <div class="container">
                    <div class="row">

                        <!-- Products -->
                        <div class="col-md-12">
                            <div class="product-detail">
                                <div class="product">
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            <div class="product product-single">
                                                <article> <img class="img-responsive" src="<?=$obj->base_url?>admin/uploads/images/<?= $product['image'] ?>"
                                                        alt=""> <img class="sale-tag" src="<?= $obj->base_url ?>img/servicepack.png">
                                                    
                                                    <div class="product-btns">
                                                        <div class="price" onclick="return addtocart(<?= $product['id'] ?>,'add')"><img src="<?= $obj->base_url ?>img/shopping-bag.png">Add to Cart
                                                        </div>
                                                        <div class="cart-ico">
                                                                <a href="https://api.whatsapp.com/send?phone=+971557802100&text=Hi, I am interested about <?= $product['brand_name'] ?> <?= $product['model_name'] ?> <?= $product['series_name'] ?> <?= $product['part_number'] ?>  LCD <?= $product['color_name'] ?>. Please let me know details" target="_blank" class="share-btn">
                                                                <img src="<?= $obj->base_url ?>img/whatsapp.png">Quick Enquiry
                                                                </a>
                                                        </div>
                                                    </div>

                                                </article>
                                            </div>
                                        </div>
                                        <!-- Item Content -->
                                        <div class="col-md-6">
                                            <h5><?php echo $product['name']; ?></h5>
                                            <p>Service Pack Online Company is well known for providing excellent
                                                customer
                                                service. The business has a committed group of qualified experts that
                                                are
                                                committed to helping clients with the ordering process, answering
                                                questions,
                                                and offering professional advice as needed. The company is a dependable
                                                partner for companies in the mobile phone sector because to prompt
                                                responses, effective order administration, and a focus on customers.
                                            </p>
                                            <!-- List Details -->
                                            <ul>
                                                <li><b>Brand : </b><?php echo $product['brand_name']; ?></li>
                                                <li><b>Model : </b> <?php echo $product['model_name']; ?></li>
                                                <li><b>Part Number : </b><?php echo $product['part_number']; ?></li>
                                                <li><b>Quality : </b><?php echo $product['quality']; ?></li>
                                                <li><b>Packing : </b> <?php echo $product['packing']; ?></li>
                                                <li><b>Parts : </b><?php echo $product['parts']; ?></li>
                                                <li><b>Color : </b> <?php echo $product['color_name']; ?></li>
                                                <li><b>Condition : </b><?php echo $product['condition']; ?></li>
                                            </ul>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">


                    <!-- heading -->
                    <div class="heading">
                        <h2>Product information</h2>

                    </div>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since
                        the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                        specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged. It was
                        popularised in the 1960s with the release of Letraset
                        sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                        Aldus PageMaker including
                        versions of Lorem Ipsum.
                    </p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since
                        the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                        specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged. It was
                        popularised in the 1960s with the release of Letraset
                        sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                        Aldus PageMaker including
                        versions of Lorem Ipsum. </p>
                    <div class="heading padding-top-20">
                        <h2>Related Products </h2>
                    </div>

                    <div class="tab-content padding-top-20 padding-bottom-40">
                        <!-- TV & Audios -->
                        <div role="tabpanel" class="tab-pane active fade in" id="tv-au">
                            
                            <!-- Items -->
                            <div class="item-slide-5 with-bullet no-nav">
                                <!-- Product -->
                                <?php
                                while ($row = mysqli_fetch_assoc($related_products)) {
                                    ?>
                                <div class="product">
                                    <article> <a href="product-details?id=<?= $row['productid'] ?>"><img class="img-responsive" src="<?=$obj->base_url?>admin/uploads/images/<?= $row['image'] ?>" alt=""> <img class="sale-tag" src="<?= $obj->base_url ?>img/servicepack.png"></a>
                                        <div class="product-name">
                                            <!-- <span class="tag"><a href="product-details?id=<?= $row['id'] ?>"><?= $row['brand_name'] ?> </a></span> <a href="product-details?id=<?= $row['productid'] ?>" class="title"><?= $row['productname'] ?></a> -->
                                            <a href="product-details?id=<?= $row['id'] ?>"><span class="tag"><?= $row['brand_name'] ?> <?= $row['series_name'] ?>.</a></span> <span class="tittle"><a href="product-details?id=<?= $row['id'] ?>">Samsung Galaxy A33 5G 2022 (SM-A336B) Lcd Display Unit
                                                    Complete White +Btry GH82-28145B</a></span>
                                            </a>
                                        </div>
                                        <div class="product-btns">
                                            <div class="price" onclick="return addtocart(<?= $row['productid'] ?>,'add')"><img src="<?= $obj->base_url ?>img/shopping-bag.png" >Add to Cart</div>
                                            <div class="cart-ico">
                                                <a href="https://api.whatsapp.com/send?phone=+971557802100&text=Hi, I am interested about <?= $row['brand_name'] ?> <?= $row['brandmodel'] ?> <?= $row['series_name'] ?> <?= $row['part_number'] ?>  LCD <?= $row['color_name'] ?>. Please let me know details" target="_blank" class="share-btn">
                                                    <img src="<?= $obj->base_url ?>img/whatsapp.png">Quick Enquiry
                                                </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                                <?php } ?>

                            </div>
                        </div>


                    </div>

                </div>

            </section>

        </div>
        <!-- End Content -->

       <!-- Footer -->
       <?php include("includes/footer.php"); ?>

    </div>
    <!-- End Page Wrapper -->
    <?php include("includes/foot.php"); ?>

</html>