<?php
include("admin/db/connection.php");
$obj = new servicepack();
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>Service Pack | Brand </title>
    <?php include("includes/head.php"); ?>
</head>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">

        <!-- Header -->
        <?php include("includes/header.php"); ?>

        <section>
            <div class="aboutus-banner"
                style="background:linear-gradient(0deg, #222222, rgba(255,255,255,.15)), url(img/about/aboutus.jpg) no-repeat">
                <div class="container text-center position-center-center">
                    <h1>FAQ</h1>

                </div>
            </div>
        </section>
        <!-- Content -->

        <style>

        </style>

        <section class="padding-top-80 padding-bottom-80">
            <div class="container">
                <div class="center-heading text-center">
                    <h2>Frequently Asked Questions</h2>
              
                </div>
                <div class="accordion">
                    </div>
                    <div class="accordion-item active">
                        <div class="accordion-header">Q : What products does Servicepack offer?</div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : We offers original display screens and spare parts for Samsung, Huawei, Xiaomi, HTC, and iPhone smartphones. 
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : Are the spare parts provided by Servicepack Company genuine and of high quality? </div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : Yes, Every one of our spare parts is authentic and of the highest calibre. We make sure that they are of the highest calibre by sourcing them from dependable vendors.  
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : What is the warranty period for Servicepack Company's products?</div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : We offer a one-year warranty on all our products, starting from the date of purchase. This warranty covers manufacturing defects and faulty parts. 
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : How long does it take to deliver the products? </div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : For air shipments, we aim to deliver within 7 days. For shipments by ship, the estimated delivery time is 15 days. 
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : Does Servicepack Company provide shipping to all countries?</div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : Yes, we do ship internationally. We can ship our goods to any nation that offers shipping services.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : Can I place a bulk order for spare parts? </div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : Yes, we accept bulk orders. Please contact our sales team or customer support for assistance with your bulk order requirements. Mail us at <a href="mailto:support@servicepack.online"> support@servicepack.online </a>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : What payment methods does Servicepack Company accept? </div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : We accept payments via authorized payment gateways, such as credit cards, debit cards, and online banking. We prioritize secure payment options. 
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : What should I do if I receive a defective or damaged product? </div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : In the rare event that you receive a defective or damaged product, please contact our customer support within 7 days of delivery. We will arrange for a refund or replacement.  Mail us at <a href="mailto:support@servicepack.online"> support@servicepack.online </a>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : What if the spare part I ordered is not compatible with my device? </div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : Before making a purchase, please verify that the spare part is compatible with your device. Please contact our customer support if you have encounter any issues. 
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : Can I track my order? </div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : Yes, you will receive a tracing number after your order has been shipped so you can monitor the progress of your order. 
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : What if I change my mind after placing an order? </div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : If you change your mind before your order is shipped, please contact our customer support as soon as possible. We will do our best to accommodate your request.  
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : Can I cancel my order? </div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : Cancellations of orders are subject to certain conditions. If you need assistance canceling your order, please contact our customer support team. It is important to note that cancellation is not possible if the order has already been shipped.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : Can I return a product if I am not satisfied? </div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : Returns are accepted within 7 days of delivery if the product is defective or damaged. In spite of this, we do not accept returns for products that have been mishandled or damaged as a result of improper installation or use.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header">Q : How can I contact Servicepack Company for further assistance?</div>
                        <div class="accordion-body">
                            <div class="accordion-body-content">
                            A : You can reach our customer support team via email at <a href="mailto:support@servicepack.online"> support@servicepack.online </a>or by phone at <a href="tel:+971 43343444">+971 43343444</a>,<a href="tel:+971 557802100"> +971 557802100 </a>. We are here to assist you with any queries or concerns you may have.
                            </div>
                        </div>
                    </div>
                    
            </div>
        </section>

        <!-- End Content -->

        <script>
        const accSingleTriggers = document.querySelectorAll(".accordion-header");

        accSingleTriggers.forEach((trigger) =>
            trigger.addEventListener("click", toggleAccordion)
        );

        function toggleAccordion() {
            const items = document.querySelectorAll(".accordion-item");
            const thisItem = this.parentNode;

            items.forEach((item) => {
                if (thisItem == item) {
                    thisItem.classList.toggle("active");
                    return;
                }
                item.classList.remove("active");
            });
        }
        </script>
        <!-- Footer -->
        <?php include("includes/footer.php"); ?>

    </div>
    <!-- End Page Wrapper -->
    <?php include("includes/foot.php"); ?>

</html>