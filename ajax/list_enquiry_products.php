<?php
include ("../admin/db/connection.php");
$obj = new servicepack();
if (isset($_REQUEST['term'])) {


    $display_name = $_REQUEST['term'];
    echo $display_name;
    exit;
    $search_array = [$display_name];
    $search_explode = preg_split('/\s+/', $display_name, -1, PREG_SPLIT_NO_EMPTY);

    if (count($search_explode) > 1) {
        $search_array = array_merge($search_array, $search_explode);
    }

    foreach ($search_array as $display_name) {
        $result = $obj->select_all_products_by_displaynames($display_name);

        $return_arr = array();
?>

        <form method="post" id="formId">
            <div class="wishlist-table-content">
                <div class="table-content table-responsive">
                    <table>
                        <thead>
                            <tr>
                                <th class="width-remove"></th>

                                <th class="width-name">Products</th>

                            </tr>
                        </thead>

                        <?php
                        while ($product = mysqli_fetch_array($result)) {

                        ?>
                            <tbody>
                                <tr>
                                    <td class="product-remove"><input type="checkbox" class="products_id" name="product_id[]" id="productid" value="<?php echo $product['id']; ?>"></td>
                                    <td class="product-name">
                                        <?php echo $product['model_name']; ?> <small><i>(<?php echo $product['display_name']; ?>)</i></small>
                                    </td>
                                </tr>
                            </tbody>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div class="form-input-item mb-0" style="padding: 25px;text-align: center;">
                <button class="btn btn-theme show-btn" disabled type="button" onclick="addProducts('addproducts')">ADD</button>
            </div>
        </form>
<?php

        if (count($return_arr) >= 0)
            break;
    }
}
?>