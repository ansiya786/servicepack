<?php
include ("../admin/db/connection.php");
$obj = new servicepack();

if (isset($_GET['term'])) {
     
    
  $display_name = $_GET['term'];
    
  $search_array = [$display_name];
  $search_explode = preg_split('/\s+/', $display_name, -1, PREG_SPLIT_NO_EMPTY);
   
  
  if(count($search_explode)>1){
      $search_array = array_merge($search_array, $search_explode);
  }


  foreach($search_array as $display_name){
    $result = $obj->select_all_products_by_displayname($display_name);

    $return_arr = array();

      while ($product = mysqli_fetch_array($result)) {

        $data['id'] = $product['id'];
        $data['value'] = $product['brand_name']." ".$product['model_name']." (".$product['focus_name'].")";
        array_push($return_arr, $data); 
      
      }
      if(count($return_arr)>=0)
        break;
     
    }

     //return json res
     echo json_encode($return_arr);
 }
?>

