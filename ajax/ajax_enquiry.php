<?php
include ("../admin/db/connection.php"); 
$obj = new servicepack();
require "../includes/mail.php";

// $data['enq_no']=$obj->getSerialNo('tbl_enquiry');
$data['name']=$_POST['name'];
$data['email']=$_POST['email'];
$data['location']=$_POST['location'];
$data['phone']=$_REQUEST['phone'];
$cookie_data = stripslashes($_COOKIE['shopping_cart']);
$cart_data = json_decode($cookie_data, true);

$item_data['parent_id']=$obj->insert_data('tbl_enquiry',$data);
$update_data['enq_no']= "ENQ-".$item_data['parent_id'];
$obj->update_data("tbl_enquiry",$update_data,$item_data['parent_id']); 
$product_details = "";
foreach($cart_data as $keys => $values)
{
    $item_data['product_id']=$values['item_id'];
    $item_data['quantity']=$values['item_quantity'];
    $enq_item_id=$obj->insert_data('tbl_enquiry_item',$item_data);
    $product = $obj->select_all_products_by_id('tbl_product',$item_data['product_id']);
    if (!empty($product)) {
        $item_name = stripslashes($product['focus_name']);
        $item_brand = stripslashes($product['brand_name']);
        $item_model = stripslashes($product['model_name']);
    }
    $product_details .= "<tr style='background: #eee;'><td>" . strip_tags($item_name) . "</td><td>" . strip_tags($item_brand) . "</td><td>" . $item_model . "</td><td>" . $item_data['quantity'] . "</td></tr>";
}

if($enq_item_id>0)
{
    $subject = 'SMATT Enquiry Details';    
    $subject2 = 'Thank you for your enquiry!';
    $message2 = '<html><body>';
    $message2 .= '<h3>Product Details</h3>';
    $message2 .= '<table rules="all" style="border-color: #666;" cellpadding="10" width:"100%">';
    $message2 .= "<tr style='background: #eee;'><th>Focus Name</th><th>Brand</th><th>Model</th><th>Quantity</th></tr>";
    $message2 .= $product_details;
    $message2 .= "</table>";
    $message2 .= "</br>";
    $message2 .= 'We will contact you soon...!!!';
    $message2 .= "</body></html>";
    $emailTo = "info@henzer.online";
    $email = $data['email'];
    $name = $data['name'];
    $headers = "MIME-Version: 1.1";
    $headers.= "Content-type: text/html; charset=iso-8859-1";
    $headers.= "From: " . $data['email'] . "\r\n"; 
 
    $headers.= "Return-Path:" . "From:" . $data['email'];
    $message = '<html><body><h1>Hello, SMATT!</h1>';
    $message .= '<h3>Contact Details</h3>';
    $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
    $message .= "<tr style='background: #eee;'><td><strong>Enquiry No:</strong> </td><td>" . $update_data['enq_no'] . "</td></tr>";
    $message .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>" . strip_tags($data['name']) . "</td></tr>";
    $message .= "<tr style='background: #eee;'><td><strong>Email:</strong> </td><td>" . strip_tags($data['email']) . "</td></tr>";
    $message .= "<tr style='background: #eee;'><td><strong>Location:</strong> </td><td>" . strip_tags($data['location']) . "</td></tr>";
    $message .= "<tr style='background: #eee;'><td><strong>Phone:</strong> </td><td>" . strip_tags($data['phone']) . "</td></tr>";
    $message .= "</table>";
    $message .= '<h3>Product Details</h3>';
    $message .= '<table rules="all" style="border-color: #666;" cellpadding="10" width:"100%">';
    $message .= "<tr style='background: #eee;'><th>Focus Name</th><th>Brand</th><th>Model</th><th>Quantity</th></tr>";
    $message .= $product_details;
    $message .= "</table>";
    $message .= "</body></html>";
    
    if (sendmail('no-reply@henzer.online','',$emailTo,'SMATT', $subject, $message) == 1)
    {
        $success = sendmail($emailTo,'SMATT',$email,$name, $subject2 , $message2);   
        $msg = 'sent';
    } else {
        $msg = 'failed';
    }
}          
if($msg = 'sent')
{
    unset($_COOKIE['shopping_cart']);
    setcookie('shopping_cart', '', time() - 3600, '/'); // empty value and old timestam
    echo 1;
}
else {
    echo 0;
}

?>