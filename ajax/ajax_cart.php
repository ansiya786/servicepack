<?php
include ("../admin/db/connection.php");
$obj = new servicepack();

if (isset($_POST["product_id"]) && $_POST["action"] == "add") {

    $product = $obj->select_all_products_by_id('tbrl_products',$_POST["product_id"]);
    if (!empty($product)) {
        $item_name = stripslashes($product['name']);
        $item_brand = $product['brand_name'];
        $item_image =
            $obj->base_url . "/admin/uploads/images/" . $product['image'];
        $item_model = $product['model_name'];
    }
    $cart_items[$_POST["product_id"]] = [
        'item_id' => $_POST["product_id"],
        'item_name' => $item_name,
        'item_brand' => $item_brand,
        'item_image' => $item_image,
        'item_model' => $item_model,
        'item_qty' => 1,
    ];
    // read
    $cookie = isset($_COOKIE['cart_items_cookie'])
        ? $_COOKIE['cart_items_cookie']
        : "";
    $cookie = stripslashes($cookie);
    $saved_cart_items = json_decode($cookie, true);

    // if $saved_cart_items is null, prevent null error
    if (!$saved_cart_items) {
        $saved_cart_items = [];
    }
    if (array_key_exists($_POST["product_id"], $saved_cart_items)) {
        echo count($saved_cart_items).'_Item already exist in the cart!';
    } else {
        if (count($saved_cart_items) > 0) {
            foreach ($saved_cart_items as $key => $value) {
                // add old item to array, it will prevent duplicate keys
                $cart_items[$key] = [
                    'item_id' => $value['item_id'],
                    'item_name' => $value['item_name'],
                    'item_brand' => $value['item_brand'],
                    'item_image' => $value['item_image'],
                    'item_model' => $value['item_model'],
                    'item_qty' => 1,
                ];
            }
        }

        // put item to cookie
        $json = json_encode($cart_items, true);
        setcookie("cart_items_cookie", $json, time() + 86400 * 30, '/'); // 86400 = 1 day
        $_COOKIE['cart_items_cookie'] = $json;
        echo count($cart_items).'_';
    }
    die();
} elseif (isset($_POST["product_id"]) && $_POST["action"] == "delete") {
    // get the product id
    $id = isset($_POST['product_id']) ? $_POST['product_id'] : "";
    // read
    $cookie = $_COOKIE['cart_items_cookie'];
    $cookie = stripslashes($cookie);
    $saved_cart_items = json_decode($cookie, true);

    // remove the item from the array
    unset($saved_cart_items[$id]);

    // delete cookie value
    unset($_COOKIE["cart_items_cookie"]);

    // empty value and expiration one hour before
    setcookie("cart_items_cookie", "", time() - 3600);

    // enter new value
    $json = json_encode($saved_cart_items, true);
    setcookie("cart_items_cookie", $json, time() + 86400 * 30, '/'); // 86400 = 1 day
    $_COOKIE['cart_items_cookie'] = $json;
    echo count($saved_cart_items);
} 
?>
