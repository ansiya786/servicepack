<?php
include("admin/db/connection.php");
$obj = new servicepack();
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Service Pack | About Us</title>
    <?php include("includes/head.php"); ?>
</head>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">

        <!-- Header -->
        <?php include("includes/header.php"); ?>

        <section>
            <div class="aboutus-banner" style="background:linear-gradient(0deg, #222222, rgba(255,255,255,.15)), url(img/about/aboutus.jpg) no-repeat">
                <div class="container text-center position-center-center">
                    <h1>About - Us</h1>
                    <h2>Service Pack LCD</h2>
                </div>
            </div>
        </section>
        <!-- Content -->

        <section class="padding-top-80">
            <div class="container">


                <!-- heading -->
                <div class="heading">
                    <h2>About Us</h2>

                </div>
                <p>Service Pack Company was established in.... in ....... , as a business that offers top-notch
                    goods,
                    genuine displays, and other Smartphone replacements. We are the leading
                    international wholesaler and distributor of popular mobile phone brands for the consumer and
                    business markets. The business serves customers globally and specializes in
                    managing large orders. It has a solid reputation for offering authentic spare parts and
                    accessories.
                </p>
                <p>We have coordinated our efforts to support the digital revolution and increase customer
                    satisfaction
                    by offering premium, genuine OLED and AMOLED displays and other spare
                    parts that are 100% compatible, a real fix for your Smartphone display and other problems! </p>
                <p>We have an expert workforce that is well-trained and passionate, and our clientele is
                    well-managed
                    throughout the Middle East, Europe, South Africa, and other parts of the world.</p>

            </div>
        </section>
        <section class="padding-top-50">
            <div class="container">

                <!-- heading -->
                <div class="center-heading text-center">
                    <h2>Why You Should Choose Service Pack Online?</h2>
                    <h5>Service Pack LCD</h5>
                </div>


                <p class="shot-desc">With a focus on quality, efficient bulk order handling, and exceptional customer service, Service
                    pack Online has become a reliable partner for businesses and repair professionals worldwide.
                </p>
                <div class="about-service mt-30">
                    <div class="col-md-6 about-service-two">
                        <div class="blog-post">
                            <img class="img-responsive" src="<?= $obj->base_url ?>img/about/about-Group1.jpg" alt="">
                            <p class="text-center"><b>Bulk Order Handling</b></p>
                            <p class="text-center">Handling bulk orders is a specialty of Service Pack Online Company, which serves merchants,
                                enterprises, and repair facilities with significant needs. The business has the systems in place
                                and the logistics expertise to effectively handle and fulfill large orders, providing prompt
                                and affordable supply to its clients.</p>
                        </div>
                    </div>
                    <div class="col-md-6 about-service-two">
                        <div class="blog-post">
                            <img class="img-responsive" src="<?= $obj->base_url ?>img/about/about-Group2.jpg" alt="">
                            <p class="text-center"><b>Quality Assurance</b></p>
                            <p class="text-center">The Service Pack Online Company is committed to giving its clients high-quality items. Authenticity and dependability are ensured by the company's direct sourcing of spare parts and accessories from manufacturers and authorized distributors. To uphold the company's dedication to excellence, stringent quality control procedures are used to confirm the reliability and effectiveness of every product.</p>
                        </div>
                    </div>
                    <div class="justify-content-center about-service-one">
                        <div class="blog-post">
                            <img class="img-responsive" src="<?= $obj->base_url ?>img/about/about-Group3.jpg" alt="">
                            <p class="text-center"><b>Customer Support</b></p>
                            <p class="text-center">Service Pack Online Company is well known for providing excellent customer service. The business has a committed group of qualified experts that are committed to helping clients with the ordering process, answering questions, and offering professional advice as needed. The company is a dependable partner for companies in the mobile phone sector because to prompt responses, effective order administration, and a focus on customers.</p>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="padding-top-20 padding-bottom-50">
            <div class="container">


                <!-- heading -->
                <div class="heading">
                    <h2>Our Team</h2>

                </div>
                <p>At the core of the Service Pack Company's success is their exceptional team, comprising dedicated professionals who possess extensive knowledge and experience in the
                    mobile phone industry. Each team member is essential to providing outstanding customer service and guaranteeing that customers receive the best goods and services.
                </p>
                <p>The team makes an effort to deliver individualized service, swiftly responding to client questions and concerns, and providing custom solutions to fit their particular needs.
                    The collaborative approach allows the team to tackle complex projects effectively and efficiently, ensuring that all aspects of the business, from procurement to delivery,
                    are seamlessly managed. </p>

                <div class="about-bottom margin-top-40 margin-bottom-40">
                    <img class="img-responsive" src="<?= $obj->base_url ?>img/about/aboutus-bottom.jpg" alt="">

                </div>

                <p>Since they are aware that trust and dependability are crucial for enduring connections, they place a high priority on developing strong and enduring relationships with their
                    clientele. The team strives to provide personalized service, addressing client queries and concerns promptly and offering tailored solutions to meet their unique needs.
                </p>
                <p>The Service Pack Company's team is a powerhouse of talent and expertise, driven by a commitment to providing quality products, efficient project management, and
                    exceptional client service. They stand out as a top global wholesaler and distributor in the mobile phone sector thanks to their commitment to teamwork, best project
                    management techniques, and client management.</p>
            </div>
        </section>

        <?php include("includes/footer.php"); ?>

    </div>
    <!-- End Page Wrapper -->
    <?php include("includes/foot.php"); ?>

</html>