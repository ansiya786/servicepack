<?php
include("admin/db/connection.php");
$obj = new servicepack();
$words = explode(' ', $_SERVER['REQUEST_URI']);
$showword = trim($words[count($words) - 1], '/');
echo basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>Service Pack | Home</title>
    <?php include("includes/head.php"); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
</head>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">

        <!-- Header -->
        <?php include("includes/header.php"); ?>


        <section class="padding-top-40 padding-bottom-60">
            <div class="container">
                <div class="center-heading text-center">
                    <h2 class="main-heading">
                        <p>LATEST PRODUCTS</p><span>Service Pack Lcd.</span>
                    </h2>
                    <hr class="latest-head-line">
                </div>
                <div class="row product-list  padding-top-40">
                    <!-- Products -->
                    <div class="col-md-12 padding-bottom-60 product-div">
                        <!-- Items -->
                        <div class="item-col-4">
                            <!-- Product -->
                            <?php
                            $count = $obj->getAllProductsCount();
                            $result = $obj->getAllProducts(0, 12);
                            while ($row = mysqli_fetch_assoc($result)) {
                            ?>
                                <div class="product">
                                    <article>
                                        <a href="product-details?id=<?= $row['id'] ?>"><img class="img-responsive" src="<?= $obj->base_url ?>admin/uploads/images/<?= $row['image'] ?>" alt=""> <img class="sale-tag" src="<?= $obj->base_url ?>img/servicepack.png"></a>
                                        <div class="product-name">
                                            <!-- <span class="tag"><a href="product-details?id=<?= $row['id'] ?>"><?= $row['brand_name'] ?> </a></span> <a href="product-details?id=<?= $row['productid'] ?>" class="title"><?= $row['productname'] ?></a> -->
                                            <a href="product-details?id=<?= $row['id'] ?>"><span class="tag"><?= $row['brand_name'] ?> <?= $row['series_name'] ?>.</a></span> <span class="tittle"><a href="product-details?id=<?= $row['id'] ?>">Samsung Galaxy A33 5G 2022 (SM-A336B) Lcd Display Unit
                                                    Complete White +Btry GH82-28145B</a></span>
                                            </a>
                                        </div>
                                        <div class="product-btns">
                                            <div class="price" onclick="return addtocart(<?= $row['id'] ?>,'add')"><img src="<?= $obj->base_url ?>img/shopping-bag.png">Add to Cart</div>
                                            <div class="cart-ico">
                                                <a href="https://api.whatsapp.com/send?phone=+971557802100&text=Hi, I am interested about <?= $row['brand_name'] ?> <?= $row['model_name'] ?> <?= $row['series_name'] ?> <?= $row['part_number'] ?>  LCD <?= $row['color'] ?>. Please let me know details" target="_blank" class="share-btn">
                                                    <img src="<?= $obj->base_url ?>img/whatsapp.png">Quick Enquiry
                                                </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                            <?php
                            }
                            ?>
                        </div>


                    </div>
                    <div class="btn-center products">
                        <a class="btn-round">
                            <img src="<?= $obj->base_url ?>img/Icon feather-loader.png">
                            <span class="load-more">Load Next Products</span></a>


                        <input type="hidden" id="row" value="0">
                        <input type="hidden" id="all" value="<?php echo $count; ?>">
                    </div>
                </div>
            </div>
        </section>


        <script>
            $(document).ready(function() {

                // Load more data
                $('.load-more').click(function() {
                    var row = Number($('#row').val());
                    var allcount = Number($('#all').val());
                    var rowperpage = 12;
                    row = row + rowperpage;


                    $("#row").val(row);

                    $.ajax({
                        url: '<?= $obj->base_url ?>getData.php',
                        type: 'post',
                        data: {
                            row: row
                        },
                        beforeSend: function() {
                            $(".load-more").text("Load Next Products");
                        },
                        success: function(response) {

                            // Setting little delay while displaying new content
                            setTimeout(function() {
                                // appending posts after last post with class="post"
                                $(".product:last").after(response).show().fadeIn("slow");

                                var rowno = row + rowperpage;

                                // checking row value is greater than allcount or not
                                if (rowno > allcount) {

                                    // Change the text and background
                                    $('.load-more').text("Hide");

                                } else {
                                    $(".load-more").text("Load Next Products");
                                }
                            }, 2000);

                        }
                    });
                });

            });
        </script>
        <?php include("includes/footer.php"); ?>


    </div>


    <?php include("includes/foot.php"); ?>

</html>