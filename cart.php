<?php
include("admin/db/connection.php");
$obj = new servicepack();
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>Service Pack | About Us</title>
    <?php include("includes/head.php"); ?>
</head>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">

        <!-- Header -->
        <?php include("includes/header.php"); ?>

        <section>
            <div class="aboutus-banner"
                style="background:linear-gradient(0deg, #222222, rgba(255,255,255,.15)), url(img/about/aboutus.jpg) no-repeat">
                <div class="container text-center position-center-center">
                    <h1>Enquire Your Product</h1>
                    <h2>Service Pack LCD</h2>
                </div>
            </div>
        </section>
        <!-- Content -->

        <section class="padding-top-80 padding-bottom-80">
            <div class="container">


                <div class="center-heading text-center">
                    <h3>Discover a World of Premium Smartphone LCDs,Displays, and Spare Parts - Enquire Now!</h3>
                    <p>Welcome to our smartphone LCDs,Displays, and Spare Parts inquiry platform! We are thrilled to
                        assisst you in finding the perfect product for your needs.</p>
                    <p>Please provide us with your specific requirements, and we will respond to your inquiry promptly.
                    </p>
                </div>
                <div id="succEnquiry" style="display:none;">
                    <div class="modal-dialog enq-modal" role="document">
                        <div class="modal-content">
                            <div class="modal-header forgot-pwd">
                                <button type="button" class="close tick" data-dismiss="modal" aria-label="Close">
                                    <!--<i class="fa fa-check" aria-hidden="true"></i>-->
                                    <img src="<?= $obj->base_url ?>images/tick.png" style=" width: 20px; filter: brightness(0) invert(1);">
                                </button>
                            </div>
                            <div class="modal-body">
                                <h4 class="modal-title text-center congrats-title">Congratulations.</h4>
                                <p id="cart_msg"> We appreciate you enquiring about our product. Within 24 hours, we'll
                                    respond to you. </p>
                            </div>
                            <div class="modal-footer"
                                style="text-align: center;    padding: 0px 35px 35px 35px;border-top:unset;">
                                <a href="index.php"><button type="button" class="btn btn-secondary cart-btn">Back To
                                        Home</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="enq-div">

                
                        <div class="">
                            <div style="clear:both"></div>
                            <br>
                            <div class="table-responsive">
                                <table class="table cart-table">
                                    <tbody>
                                        <tr>
                                            <th width="30%">PRODUCT</th>
                                            <th width="20%">BRAND</th>
                                            <th width="20%">MODEL</th>
                                            <th width="10%">QUANTITY</th>
                                            <th width="20%"></th>
                                        </tr>
                                        <?php
                                            $cookie = isset($_COOKIE['cart_items_cookie']) ? $_COOKIE['cart_items_cookie'] : "[]";
                                            if($cookie=="null"){
                                            $saved_cart_items = array();    
                                            }else{
                                            $cookie = stripslashes($cookie);
                                            $saved_cart_items = json_decode($cookie, true);
                                            }

                                            if (count($saved_cart_items) > 0) {
                                                $total = 0;
                                                $i = 0;
                                                foreach ($saved_cart_items as $keys => $values) {
                                            ?>
                                        <tr id="orderrow_<?php echo $values["item_id"]; ?>">
                                            <td><?php echo $values["item_name"]; ?></td>
                                            <td><?php echo $values["item_brand"]; ?></td>
                                            <td><?php echo $values["item_model"]; ?></td>
                                            <td>
                                                <p>
                                                   
                                                        <input name="name_ct[]" id="id_ct<?= $i ?>" type="number" class="form-control valid" placeholder="quantity" value="<?php echo $values["item_qty"]; ?>" >
                                                </p>
                                                <input name="item_id[]" type="hidden" value="<?php echo $values["item_id"]; ?>">
                                            </td>
                                            <td class="trash">
                                            <a id="<?php echo $values["item_id"]; ?>" class="view foo-242"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                        <?php
                                            $i++;
                                            }
                                            ?>
                                        <?php
                                            } else {
                                            ?>
                                        <tr>
                                            <td colspan="5" align="center">
                                                <p style="color: red">Your cart is empty.....</p>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php
              if (count($saved_cart_items) > 0) {
                  ?>
                        <div class="send-btn text-right">
                            <button type="submit" value="submit" class="btn-round" id="send_request"  onclick="showContactDetails()">Continue</button>
                           
                        </div>
                        <?
              }
              ?>
                    


                    <div style="clear:both"></div>
                    <div id="ContactDetails"  class="padding-top-40" style="display:none">
                        <form method="post" id="contact-reg" novalidate="novalidate">
                            <div class="product-header">

                                <div class="row text-center">

                                    <p>Contact Details</p>


                                </div>

                            </div>
                            <div class="" style="margin-top: 20px;">
                                <!--<div id="message" class="success_message"> </div>-->

                                <div class="row">
                                    <div class="col-md-3 mb-3">

                                        <input type="text" class="form-control" name="name" id="name"
                                            placeholder="your name">
                                        <label class="error" for="name" style="color: red;"></label>
                                    </div>
                                    <div class="col-md-3 mb-3">

                                        <input type="text" class="form-control" name="email" id="email"
                                            placeholder="Email">
                                        <label class="error" for="email" style="color: red;"></label>
                                    </div>

                                    <div class="col-md-3 mb-3">

                                        <input type="text" class="form-control" name="location" id="location"
                                            placeholder="Location">
                                        <label class="error" for="location" style="color: red;"></label>
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <input type="tel" class="form-input form-control" name="phone"
                                            id="phone" placeholder="Your Phone" />

                                    </div>
                                </div>


                                <div class="send-btn text-right padding-top-10">
                                    <a href="#." class="btn-round">Send Enquiry</a>
                                </div>
                            </div>

                        </form>

                    </div>

                </div>

            </div>
        </section>
        
        <!-- End Content -->

        <!-- Footer -->
        <?php include("includes/footer.php"); ?>

    </div>
    <!-- End Page Wrapper -->
    <?php include("includes/foot.php"); ?>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.min.js"></script>
    <script>

function showContactDetails() {
            $(".send-btn").hide();
            document.getElementById('ContactDetails').style.display = "block";
        }


    $('a.view').click(function() {
    var product_id = $(this).attr('id');
  
    $.ajax({
      url: "ajax/ajax_cart.php",
      method: "POST",
      data: {
        product_id: product_id,
        action: "delete"
      },
      success: function(data) {
           $("#lblCartCount").html(data);
        $("#orderrow_" + product_id).hide();
      }
    });
  });


  function updateCart(product_id, quantity) {

    var action = 'update';
    $.ajax({
        type: 'POST',
        url: "ajax/ajax_cart.php",
        data: {
            product_id: product_id,
            action: action,
            quantity: quantity
        },
        success: function(result) {
            // console.log(result);
            // window.location = "enquiry";
        }
    });
}



  var phone_number = window.intlTelInput(document.querySelector("#phone"), {
  separateDialCode: true,
  preferredCountries:["ae"],
  hiddenInput: "full",
  utilsScript: "//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/utils.js"
});
        </script>

</html>