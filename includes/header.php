

<div id="cartModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog enq-modal" role="document">
        <div class="modal-content">
            <div class="modal-header forgot-pwd">
                <!--<h5 class="modal-title text-center mod-title">Congratulations.</h5>-->
                <!--<button type="button" class="close cros" data-dismiss="modal" aria-label="Close">-->
                <!--  <i class="fa fa-shopping-cart" aria-hidden="true"></i>-->
                <!--</button>-->
                <button type="button" class="close tick" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title text-center congrats-title">Cart Details</h4>
                <p id="cart_msg"></p>
            </div>
            <div class="modal-footer" style="text-align: center;    padding: 0px 35px 35px 35px;border-top:unset;">
                <a href="<?= $obj->base_url ?>cart.php"><button type="button" class="btn btn-secondary cart-btn" style="display:none;">Go to Cart</button></a>
                <button type="button" class="btn btn-secondary cls-btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<header>
    <div class="container top-container">
        <div class="logo"> <a href="<?= $obj->base_url ?>"><img src="<?= $obj->base_url ?>img/servicepack.svg" alt="" class="main-logo"></a> </div>
        <div class="search-cate">
            <!-- <input type="search" placeholder="Search Products..."> -->
            <input class="term search_input" type="text" id="term" name="term" placeholder="Search Brand/Model..">
            <button class="submit" type="submit" onClick="search_product()"><i class="icon-magnifier"></i></button>
        </div>
        <?php
        $cookie = isset($_COOKIE['cart_items_cookie']) ? $_COOKIE['cart_items_cookie'] : "[]";
        if ($cookie == "null") {
            $saved_cart_items = array();
        } else {
            $cookie = stripslashes($cookie);
            $saved_cart_items = json_decode($cookie, true);
        }
        ?>
        <!-- Cart Part -->
        <ul class="nav navbar-right cart-pop">
            <a href="cart"><button class="submit cart nav-top" type="submit" onclick=""><i class="fa fa-shopping-cart" style="font-size:35px;"></i>
                    <span class='badge badge-warning' id='lblCartCount'><?php if (count($saved_cart_items) > 0) echo count($saved_cart_items); ?></span>
                </button></a>


        </ul>
    </div>

    <!-- Nav -->
    <nav class="navbar ownmenu">
        <div class="container mob-menu">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-open-btn" aria-expanded="false"> <span><i class="fa fa-navicon  menu-bar"></i></span> </button>
            </div>
            <!-- NAV -->
            <div class="collapse navbar-collapse off-canvas-area offcanvas offcanvas-end" id="nav-open-btn">
                <div class="mob-logo">
                    <a href="<?= $obj->base_url ?>"><img src="<?= $obj->base_url ?>img/servicepack-logo.svg" style="width: 40%;"></a>
                </div>
                <ul class="nav menu">
                    <?php
                    $result = $obj->select_all_brand('tbrl_brands');
                    while ($row = mysqli_fetch_assoc($result)) {
                    ?>
                        <li class="brands"> <a href="<?= $obj->base_url ?>products/<?= $row['name'] ?>"><?= $row['name'] ?> </a> </li>
                    <?php
                    }
                    ?>
                    <li class="separate-line"> <a>| </a> </li>
                    <li>
                        <hr class="separate-hline" />
                    </li>
                    <li> <a href="<?= $obj->base_url ?>about">About Us</a></li>
                    <li> <a href="<?= $obj->base_url ?>brand">Brand</a></li>
                    <li> <a href="<?= $obj->base_url ?>support">Support</a></li>
                </ul>
                <div class="rights mobnav-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="white-color">Copyright © <?php echo date("Y"); ?>, Ashtel Service Pack <br>  Powered By <a href="https://adagency.design/"> ADAgency.design </a> All rights reserved.</p><a href="https://adagency.design/">
                                </a>
                            </div><a href="https://adagency.design/" style="color:black">
                                <!-- <div class="col-sm-6 text-right"> <img src="images/card-icon.png" alt=""> </div>-->
                            </a>
                        </div><a href="https://adagency.design/" style="color:black">
                        </a>
                    </div><a href="https://adagency.design/" style="color:black">
                    </a>
                </div>
                <div class="close-btn">
                    <button class="close-icon"></button>
                </div>
            </div>


        </div>
    </nav>
</header>