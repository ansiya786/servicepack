<script src="<?= $obj->base_url ?>js/vendors/jquery/jquery.min.js"></script>
<script src="<?= $obj->base_url ?>js/vendors/wow.min.js"></script>
<script src="<?= $obj->base_url ?>js/vendors/bootstrap.min.js"></script>
<script src="<?= $obj->base_url ?>js/vendors/own-menu.js"></script>
<script src="<?= $obj->base_url ?>js/vendors/jquery.sticky.js"></script>
<script src="<?= $obj->base_url ?>js/vendors/owl.carousel.min.js"></script>

<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="<?= $obj->base_url ?>rs-plugin/js/jquery.tp.t.min.js"></script>
<script type="text/javascript" src="<?= $obj->base_url ?>rs-plugin/js/jquery.tp.min.js"></script>

<script src="<?= $obj->base_url ?>js/main.js"></script>
<script src="<?=$obj->base_url ?>js/alertify.min.js"></script>


<script>
  $('.close-icon').click(function() {
    $(".collapse").removeClass("in");
  });
</script>
<script src="<?= $obj->base_url ?>js/jquery-ui.js"></script>
<script type="text/javascript">
  $(function() {
    $(".search_input").autocomplete({

      source: 'ajax/ajax-db-search.php',
      select: function(event, ui) {

        product_id = ui.item.id;
        quantity = 1;
        action = 'add';

        $.ajax({
            method: "POST",
            url: "ajax/ajax_cart.php",
            data: {
              product_id: product_id,
              quantity: quantity,
              action: action
            },
          })
          .done(function(event, ui) {
            window.location.href = "#";
            // $('#term').val(ui.item.value + "\n");
          })
        return false;
      },
    });


  });
</script>

<script type="text/javascript" src="<?= $obj->base_url ?>js/cform/form-validate.js"></script>


<script src="<?= $obj->base_url ?>js/jquery.validate.min.js"></script>

<script>
  $(document).ready(function() {
    $(".error").css("color", "red");
    var posturl = '<?= $obj->base_url ?>';
    $("#subscribe_form").validate({
      rules: {
        email: {
          required: true,
          email: true,
          remote: {
            url: posturl + "ajax/checknewsletterEmail.php",
            type: "post"
          }
        }
      },
      messages: {
        email: {
          required: "Please enter email!",
          email: "This is not a valid email!",
          remote: "Email already in use!"
        }
      },
      submitHandler: function() {
        var posturl = '<?= $obj->base_url ?>';
        $.ajax({
          url: posturl + "ajax/newsletter.php",
          type: "POST",
          data: {
            email: $("#emailnews").val()
          },
          success: function(data) {
            alert(data); 
            $(".sub-slide-div").addClass("subscription-div");
            if (data == 0) {
              $("#messagen").html("The submitted email is already exist! ");
            } else {
              $("#messagen").html("Thank you for subscribing New Letter!.");
            }
            setTimeout(function() {
              $("#emailnews").val('');
              $("#messagen").html('');
              $('.sub-slide-div').removeClass('subscription-div');
            }, 1000)

          }
        });
        return false;
      }
    });
    


  });

  function search_product(){
     var val= $('.term').val();
    //  alert(val);
     str = val.replace(/\s+/g, '-');
     window.location.href="<?=$obj->base_url?>products/"+str;
   
  }


  function addtocart(product_id, action) {
    var posturl = '<?= $obj->base_url ?>';
    $.ajax({
      type: 'POST',
      url: posturl + "ajax/ajax_cart.php",
      data: {
        product_id: product_id,
        action: action
      },
      success: function(result) {
        var res = result.split("_");
        $("#lblCartCount").html(res[0]);
        if (res[1] != '') {
          $("#cart_msg").html(res[1]);
          alertify.set('notifier', 'position', 'top-right');
          alertify.success("Item already exist in the cart!");
                   
        } else {
          alertify.set('notifier', 'position', 'top-right');
          alertify.success("Item added successfully");
        }
        // $("#cartModal").modal("show");
       
        // window.location = posturl + "cart";
      }
    });
  }
</script>
</body>