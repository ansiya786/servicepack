<footer class="footer-dark">
    <div class="container">

        <!-- Footer Upside Links -->

        <div class="row padding-top-50 padding-bottom-50">

            <!-- Contact -->
            <div class="col-md-4">
                <h4><a href="<?= $obj->base_url ?>"><img src="<?= $obj->base_url ?>img/footer-logo.svg" alt=""
                            class="footer-logo"></a></h4>
                <p>Service Pack Company was established in 2014 in UAE , as a
                    business that offers top-notch goods, genuine displays, and other
                    Smartphone replacements. We are the leading international
                    wholesaler and distributor of popular mobile phone brands for the
                    consumer and business markets. The business serves customers
                    globally and specializes in managing large orders. It has a solid
                    reputation for offering authentic spare parts and accessories.</p>
            </div>

            <!-- Categories -->
            <div class="col-md-2">
                <h4>Product</h4>
                <ul class="links-footer">
                    <li><a href="<?= $obj->base_url ?>about" target="_blank"> About Us</a></li>
                    <li><a href="<?= $obj->base_url ?>delivery" target="_blank"> Delivery</a></li>
                    <li><a href="<?= $obj->base_url ?>privacy-policy" target="_blank"> Privacy Policy</a></li>
                    <li><a href="<?= $obj->base_url ?>terms-and-condition" target="_blank"> Terms & Conditions</a></li>
                    <li><a href="<?= $obj->base_url ?>support" target="_blank"> Contact Us</a></li>
                </ul>
            </div>

            <!-- Categories -->
            <div class="col-md-3">
                <h4>Address</h4>
                <P>
                    ASHTEL LLC<br>
                    Suite : 2501-2503, Latifa Tower,<br>
                    Sheikh Zayed Road, Dubai, United Arab Emirates<br>
                    Phone : <a href="tel:+971 43343444">+971 43343444</a><br>
                    Mobile : <a href="tel:+971 557802100"> +971 557802100 </a><br>
                    Email : <a href="mailto:support@servicepack.online"> support@servicepack.online </a><br>
                </P>
            </div>

            <!-- Categories -->
            <div class="col-md-3">
                <h4>Newsletter</h4>
                <p>Get the latest style updates and special deals
                    directly in your inbox</p>
                <div class="newslatter">
                    <form class="subscribe_form" id="subscribe_form" method="post">
                        <input name="email" id="emailnews" type="text" class="form-control subscribe"
                            placeholder="Enter Your Email ID..">
                        <button type="submit">Send</button>
                    </form>
                    <span id="messagen"></span>

                </div>

                <div class="social-links">
                    <a href="https://www.facebook.com/people/Service-Pack/100086236000000" target="_blank"><i
                            class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/servicepack.online" target="_blank"><i
                            class="fa fa-instagram"></i></a>
                    <a href="https://twitter.com/ServicePack19" target="_blank"><i class="fa fa-twitter"></i></a>
                </div>

            </div>
        </div>
    </div>

</footer>
<!-- Rights -->
<div class="rights dark">
    <div class="container">
        <div class="row text-center">

            <p style="color:#6e6e6e !important;">Copyright ©
                <?php echo date("Y"); ?>, Ashtel Service Pack | Powered By <a href="https://adagency.design/"
                    target="_blank" style="color:orangered;"> ADAgency.design </a> All rights reserved.
            </p>


        </div>
    </div>
</div>
<script>
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/6313280c37898912e966f79b/1gc1d8sk4';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>

<a href="https://api.whatsapp.com/send?phone=+971557802100&text=Hi..." class="float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
</a>
<!-- GO TO TOP  -->
<a href="<?= $obj->base_url ?>cart"><button class="submit cart nav-top cart-top" type="submit" onclick=""><i
            class="fa fa-shopping-cart" style="font-size:30px;"></i></button></a>
<a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a>