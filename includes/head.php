<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="M_Adnan" />
<!-- Document Title -->

<!-- Favicon -->
<link rel="shortcut icon" href="<?= $obj->base_url ?>img/ashtel-logo.svg" type="image/x-icon">
<link rel="icon" href="<?= $obj->base_url ?>img/ashtel-logo.svg" type="image/x-icon">

<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="<?= $obj->base_url ?>rs-plugin/css/settings.css" media="screen" />

<!-- StyleSheets -->
<link rel="stylesheet" href="<?= $obj->base_url ?>css/ionicons.min.css">
<link rel="stylesheet" href="<?= $obj->base_url ?>css/bootstrap.min.css">
<link rel="stylesheet" href="<?= $obj->base_url ?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?= $obj->base_url ?>css/main.css">
<link rel="stylesheet" href="<?= $obj->base_url ?>css/style.css">
<link rel="stylesheet" href="<?= $obj->base_url ?>css/responsive.css">
<link rel="stylesheet" href="<?= $obj->base_url ?>css/corporate.css" type="text/css">
<link rel="stylesheet" href="<?=$obj->base_url?>css/alertify.min.css">

<!-- Fonts Online -->
<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">


<!-- JavaScripts -->
<script src="<?= $obj->base_url ?>js/vendors/modernizr.js"></script>
