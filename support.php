<?php
include("admin/db/connection.php");
$obj = new servicepack();
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>Service Pack | Contact</title>
    <?php include("includes/head.php"); ?>
</head>

<body>

<!-- Page Wrapper -->
<div id="wrap"> 
  
<?php include("includes/header.php"); ?>
  <!-- Content -->
  <div id="content"> 
    
   
    <!-- Contact -->
    <section class="contact-sec padding-top-40 padding-bottom-80">
      <div class="container"> 
        
        <!-- MAP -->
        <section class="map-block margin-bottom-40">
        <iframe width="1140" height="350" id="gmap_canvas" src="https://maps.google.com/maps?q=dubai&amp;t=&amp;z=15&amp;ie=UTF8&amp;iwloc=&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        </section>
        
        <!-- Conatct -->
        <div class="contact">
          <div class="contact-form"> 
     
            <!-- FORM  -->
            <form  class="contact-form" type="POST" id="contact_form" onsubmit="return valid_datas( this );"> 
              <div class="row">
                <div class="col-md-8"> 
                  
                  <!-- Payment information -->
                  <div class="heading">
                    <h2>How can i help you ? </h2>
                  </div>
                  <div id="form_status"></div>
                  <ul class="row">
                    <li class="col-sm-6">
                      <label>First Name
                        <input type="text" class="form-control" name="name" id="name" placeholder="">
                      </label>
                    </li>
                    <li class="col-sm-6">
                      <label>Email
                        <input type="text" class="form-control" name="email" id="email" placeholder="">
                      </label>
                    </li>
                    <li class="col-sm-12">
                      <label>Message
                        <textarea class="form-control" name="message" id="message" rows="5" ></textarea>
                      </label>
                    </li>
                    <li class="col-sm-12 no-margin">
                    <input type="hidden" name="token" value="FsWga4&amp;@f6aw">
                      <button type="submit" value="submit" class="btn-round contact-btn" id="btn_submit">Send Message</button>
                    </li>
                  </ul>
                </div>
                
                <!-- Conatct Infomation -->
                <div class="col-md-4">
                  <div class="contact-info">
                    <h5>Ashtel</h5>
                    <p>OLED Service Pack Retailer and Distributors</p>
                    <hr>
                    <h6> Address:</h6>
                    <p>ASHTEL LLC<br>
                    Suite: 2501-2503, Latifa Tower,Sheikh Zayed Road, Dubai, United Arab Emirates</p>
                    <h6>Phone:</h6>
                    <p><a href="tel:+971 43343444">+971 43343444</a></p>
                    <p><a href="tel:+971 557802100">+971 557802100</a></p>
                    <h6>Email:</h6>
                    <p><a href="mailto:support@servicepack.online">support@servicepack.online</a></p>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    
  
  <!-- End Content --> 

        <!-- Footer -->
        <?php include("includes/footer.php"); ?>
   
    </div>
    <!-- End Page Wrapper -->
    <?php include("includes/foot.php"); ?>

</html>