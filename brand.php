<?php
include("admin/db/connection.php");
$obj = new servicepack();
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>Service Pack | Brand </title>
    <?php include("includes/head.php"); ?>
</head>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">

        <!-- Header -->
        <?php include("includes/header.php"); ?>

        <section>
            <div class="aboutus-banner" style="background:linear-gradient(0deg, #222222, rgba(255,255,255,.15)), url(img/about/aboutus.jpg) no-repeat">
                <div class="container text-center position-center-center">
                    <h1>100% Brand New, Original Spare Parts</h1>
                    <h2>Service Pack LCD</h2>
                </div>
            </div>
        </section>
        <!-- Content -->

        <section class="padding-top-80">
            <div class="container">
                <div class="subheading">
                    <h2>OLED Samrtphone Display</h2>
                    <h5>Service Pack LCD</h5>
                </div>
                <p>Every spare component offered by Servicepack Online carries the company's trademark. Only when it is
                    absolutely necessary for our customers to determine whether specific repair or replacement parts are
                    compatible with the devices they possess do we use recognized brand trademarks for cellphones;
                    otherwise, Servicepack Online produces the spare parts. We pride ourselves on delivering the best
                    and most authentic products to meet the needs of our customers. We serve well-known smartphone
                    brands including Samsung, Huawei,
                    Xiaomi, HTC, and iPhone thanks to our extensive selection of original display and replacement parts.
                </p>
                <p>The items are divided into Original AMOLED Display and Other Quality Spares categories. Each and
                    every spare part produced by Servicepack Online is examined for compatibility, quality, and
                    performance.
                </p>
                <p>The items are divided into Original AMOLED Display and Other Quality Spares categories. Each and
                    every spare part produced by Servicepack Online is examined for compatibility, quality, and
                    performance.</p>
                <div class="subheading">
                    <h2>Original Display Screens</h2>
                    <h5>Service Pack LCD</h5>
                </div>
                <p>Experience crystal-clear visuals and vibrant colors with our collection of original display screens.
                    Designed to match the exact specifications of your smartphone model, these high-quality displays
                    ensure a seamless viewing experience.
                </p>


                <div class="subheading">
                    <h2>Genuine Spare Parts</h2>
                    <h5>Service Pack LCD</h5>
                </div>
                <p>We at Servicepack Online, recognize the value of genuine accessories for your smartphone.
                    A wide range of authentic spare components, including batteries, chargers, camera modules, speakers,
                    buttons, and more, are available from us. You may be sure that because we buy our products directly
                    from the producers, your device will work with them and operate at its best. It offers a variety of
                    comprehensive smartphone spare parts, including speaker cables, ear speaker cables, charging flex,
                    front cameras, and back cameras. These parts are all constructed with cutting-edge technology and
                    are guaranteed to perform and be original.
                </p>
            </div>
        </section>
        <section class="padding-top-50 padding-bottom-50">
            <div class="container">

                <!-- heading -->
                <div class="center-heading text-center">
                    <h2>Unleash the power of perfection</h2>
                    <h5>Service Pack LCD</h5>
                </div>
                <div class="about-service">
                    <div class="col-md-6 about-service-two">
                        <div class="blog-post">
                            <img class="img-responsive" src="<?= $obj->base_url ?>img/brand/brand1.jpg" alt="">
                            <p class="text-center"><b>iPhone #Servicepacklcd</b></p>
                            <p class="text-center">We at Servicepack Online recognize the value of your iPhone and all
                                of its parts. To meet your demands, we offer a large selection of genuine iPhone spare
                                parts. Our iPhone spare parts, which range from replacement screens to batteries,
                                cameras, and connections, are painstakingly designed to guarantee compatibility and
                                excellent performance. You can rely on us to provide the quality you demand for your
                                cherished iPhone.</p>
                        </div>
                    </div>
                    <div class="col-md-6 about-service-two">
                        <div class="blog-post">
                            <img class="img-responsive" src="<?= $obj->base_url ?>img/brand/brand2.jpg" alt="">
                            <p class="text-center"><b>Samsung #Servicepacklcd</b></p>
                            <p class="text-center">If you own a Samsung smartphone and require reliable spare parts,
                                look no further. Displays, batteries, charging ports, camera modules, and other
                                authentic Samsung spare parts are available at Servicepack Online. Our Samsung
                                replacement parts are painstakingly made to guarantee a flawless fit and outstanding
                                performance, allowing you to fully restore your device.<br><br></p>
                        </div>
                    </div>
                    <div class="col-md-6 about-service-two">
                        <div class="blog-post">
                            <img class="img-responsive" src="<?= $obj->base_url ?>img/brand/brand3.jpg" alt="">
                            <p class="text-center"><b>Huawei #Servicepacklcd</b></p>
                            <p class="text-center">We provide a wide range of genuine Huawei replacement parts for users
                                of Huawei smartphones. Our Huawei spare parts are made to the highest standards of
                                quality, including replacement screens, batteries, and other crucial parts. With our
                                genuine spare parts, you may enjoy flawless functionality and return your Huawei gadget
                                to its original state.</p>
                        </div>
                    </div>
                    <div class="col-md-6 about-service-two">
                        <div class="blog-post">
                            <img class="img-responsive" src="<?= $obj->base_url ?>img/brand/brand4.jpg" alt="">
                            <p class="text-center"><b>Xiomi #Servicepacklcd</b></p>
                            <p class="text-center">Servicepack Online is your go-to source for original Xiaomi spare
                                parts.. Our products ensure an exact fit and dependable performance whether you need a
                                replacement display, battery, or any other Xiaomi component. You can quickly solve any
                                problems with your gadget and extend its lifespan with the help of our Xiaomi
                                replacement parts.</p>
                        </div>
                    </div>
                    <div class="justify-content-center about-service-one">
                        <div class="blog-post">
                            <img class="img-responsive" src="<?= $obj->base_url ?>img/brand/brand1.jpg" alt="">
                            <p class="text-center"><b>HTC #Servicepacklcd</b></p>
                            <p class="text-center">With our selection of genuine HTC replacement parts, you can keep
                                your HTC smartphone operating properly. We provide premium displays, batteries, charging
                                ports, and other parts made especially for HTC gadgets.</p>
                        </div>
                    </div>
                </div>
                <div class="text-center padding-top-30">
                    <p>By choosing Servicepack Online, you are assured of top-notch product quality, seamless
                        compatibility, and reliable performance.
                        We are committed to building trust with our customers and delivering products that exceed
                        expectations.</p>
                </div>

            </div>
        </section>

        <!-- End Content -->


        <!-- Footer -->
        <?php include("includes/footer.php"); ?>
   
    </div>
    <!-- End Page Wrapper -->
    <?php include("includes/foot.php"); ?>

</html>